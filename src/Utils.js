const extractActiveLink = (currentPath, targetLinkArray) => {
    const explodedPath = currentPath.split('/');
    const requiredPath = explodedPath.splice(-1, 1).toString();
    const linkText = targetLinkArray.find(item => {
        return item.path === requiredPath;
    });
    return linkText ? linkText.text : 'Page';
};

const PageVariants = {
    initial: {
        opacity: 0,
        x: "-100vw",
        scale: 0.8
    },
    in: {
        opacity: 1,
        x: "0",
        scale: 1
    },
    out: {
        opacity: 0,
        x: "100vw",
        scale: 1.2
    }
};

const PageTransition = {
    type: 'tween',
    ease: 'anticipate',
    duration: 2
};

export { extractActiveLink, PageVariants, PageTransition };
