import axios from "axios";
import LocalStorage, {LocalStorageItems} from "./localstorage";
import isObject from "./utilFns";

const handleError = (errorResponseData) => {

    if (!errorResponseData) {
        return;
    }

    if (isObject(errorResponseData)) {
        let error = '';

        for (const [, val]  of Object.entries(errorResponseData)) {
            if (Array.isArray(val)) {
                error += `${val[0]} \n`;
            } else {
                if (errorResponseData.message) {
                    throw new Error(errorResponseData.message);
                }
                if (errorResponseData.code === 401 && errorResponseData.message === 'Unauthenticated') {
                    LocalStorage.removeItem(LocalStorageItems.accessToken);
                    LocalStorage.removeItem(LocalStorageItems.currentUser);
                }
            }
        }

        if (error.length > 0) {
            throw new Error(error);
        }
    }
}

const HttpClient = () => {
    const baseUrl = process.env.REACT_APP_BASE_URL;
    const instance = axios.create({
        baseURL: baseUrl
    });

    const authorizationToken = LocalStorage.getItem(LocalStorageItems.accessToken);
    if (authorizationToken) {
        instance.defaults.headers.common['Authorization'] = `Bearer ${authorizationToken}`;
    }
    instance.defaults.headers.common['Content-Type'] = 'application/json';
    instance.defaults.headers.common['Accept'] = 'application/json';

    instance.interceptors.response.use(
        res => res,
        error => {
            // console.log(error.response);
            handleError(error.response.data);
            // throw new Error(error?.response?.data?.message);
        }
    );

    return instance;
};

export default HttpClient;
