// const baseUrl = process.env.REACT_APP_BASE_URL;
const baseUrl = `http://127.0.0.1:8000/api`;

console.log('BASE URL ', baseUrl);

const fetchData = async (url) => {
    const res =  await fetch(`${baseUrl}${url}`);
    return res.json();
};

export default fetchData;
