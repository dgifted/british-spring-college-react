export const LocalStorageItems = {
    accessToken: 'accessToken',
    currentUser: 'currentUser',
    returnPath: 'returnPath'
};

const storageItem = (itemLabel, value) => {
    localStorage.setItem(itemLabel, value);
};

const getItem = (itemLabel) => {
    return localStorage.getItem(itemLabel) || '';
};

const removeItem = (itemLabel) => {
    localStorage.removeItem(itemLabel);
};

const LocalStorage = {};
LocalStorage.storeItem = storageItem;
LocalStorage.getItem = getItem;
LocalStorage.removeItem = removeItem;

export default LocalStorage;
