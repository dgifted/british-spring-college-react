import React from 'react';
import {Switch, Route, useLocation} from 'react-router-dom';
import {AnimatePresence} from 'framer-motion';
import NavBar from "./Components/NavBar";
import Footer from "./Components/Footer";
import INDEX from './Components';
import About from './Components/Pages/About';
import Admission from "./Components/Pages/Adimission";
import Academics from "./Components/Pages/Academics";
import Life from "./Components/Pages/Life";
import NotFound from "./Components/Pages/NotFound";
import {ToastProvider} from "react-toast-notifications";
import {QueryClient, QueryClientProvider} from 'react-query';

const queryClient = new QueryClient();

function App() {
    const location = useLocation();

    return (
        <ToastProvider>
            <QueryClientProvider client={queryClient}>
                <NavBar/>
                <AnimatePresence>
                    <Switch location={location} key={location.pathname}>
                        <Route path='/' exact component={INDEX}/>
                        <Route path={'/about'}>
                            <About/>
                        </Route>
                        <Route path={'/admission'}>
                            <Admission/>
                        </Route>
                        <Route path={'/academics'}>
                            <Academics/>
                        </Route>
                        <Route path={'/life'}>
                            <Life/>
                        </Route>

                        <Route path={'*'}>
                            <NotFound/>
                        </Route>
                    </Switch>
                </AnimatePresence>
                <Footer/>
            </QueryClientProvider>
        </ToastProvider>
    )
}

export default App
