import React, {useState, useEffect} from 'react';
import {Link, NavLink} from "react-router-dom";
import LOGO from "../Assets/Logo-img.png"
import Face from "../Assets/Group 10.svg"
import Tweet from "../Assets/Group 9.svg"
import Insta from "../Assets/Group 489.svg"
import "./NavBar.css";
import NavigationOverlay from "./NavigationOverlay";
import NAVLINKS from "../NavLinksData";


function NavBar() {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);
    const [isMenuOverlayShown, setIsMenuOverlayShown] = useState(false);

    const extractLinksFromData = (linkLabel) => {
        switch (linkLabel) {
            case 'About BSC':
                return NAVLINKS.about.map((menu, index) => (
                    <Link key={index} to={`/about/${menu.path}`}>{menu.text}</Link>));
            case 'Admission':
                return NAVLINKS.admission.map((menu, index) => (
                    <Link key={index} to={`/admission/${menu.path}`}>{menu.text}</Link>));
            case 'Academics':
                return NAVLINKS.academics.map((menu, index) => (
                    <Link key={index} to={`/academics/${menu.path}`}>{menu.text}</Link>));
            case 'Life at BSC':
                return NAVLINKS.life.map((menu, index) => (
                    <Link key={index} to={`/life/${menu.path}`}>{menu.text}</Link>));
        }
    }

    const populateNavLinks = (linkLabel) => {
        return extractLinksFromData(linkLabel);
    };

    let timeOut = undefined;

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if (window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    };

    useEffect(() => {
        showButton();
    }, []);

    const onOverLay = (eventTargetInnerText) => {
        switch (eventTargetInnerText) {
            case 'About BSC':
                return true;
            case 'Admission':
                return true;
            case 'Academics':
                return true;
            case 'Life at BSC':
                return true;
            default:
                return false;
        }
    }
    const [overlayContent, setOverlayContent] = useState(<p>No link for the selected menu item</p>);
    const onMouseEnterHandler = (event) => {
        if (isMenuOverlayShown) {
            clearTimeout(timeOut);
            setIsMenuOverlayShown(false);
        }
        setIsMenuOverlayShown(true);
        if (onOverLay(event.target.innerText.trim())) {
            setOverlayContent(populateNavLinks(event.target.innerText.trim()));
        }
    };
    const onMouseLeaveHandler = (event) => {
        timeOut = setTimeout(() => {
            setIsMenuOverlayShown(false);
        }, 900);
    };

    window.addEventListener("resize", showButton);

    return (
        <div className={'nav-section__wrapper'}>
            <nav className="navbarCont container-lg">
                <div className="navbarContain">
                    <Link to="/" className="navbarLogo" onClick={closeMobileMenu}>
                        <img src={LOGO} alt="logo" className="img-fluid loggo"/>
                    </Link>
                    <div className="menu-icon" onClick={handleClick}>
                        <i className={click ? "fas fa-times" : "fas fa-bars"}/>
                    </div>
                    <ul className={click ? "nav-menu active" : "nav-menu"}>
                        <li className="nav-item">
                            <NavLink to="/about"
                                     activeClassName={'active-link'}
                                     className="nav-links"
                                     onClick={closeMobileMenu}
                                     onMouseEnter={onMouseEnterHandler}
                                     onMouseLeave={onMouseLeaveHandler}>
                                About BSC
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/admission"
                                     activeClassName={'active-link'}
                                     className="nav-links"
                                     onClick={closeMobileMenu}
                                     onMouseEnter={onMouseEnterHandler}
                                     onMouseLeave={onMouseLeaveHandler}>
                                Admission
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/academics"
                                     activeClassName={'active-link'}
                                     className="nav-links"
                                     onClick={closeMobileMenu}
                                     onMouseEnter={onMouseEnterHandler}
                                     onMouseLeave={onMouseLeaveHandler}>
                                Academics
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/life"
                                     activeClassName={'active-link'}
                                     className="nav-links"
                                     onClick={closeMobileMenu}
                                     onMouseEnter={onMouseEnterHandler}
                                     onMouseLeave={onMouseLeaveHandler}>
                                Life at BSC
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            {<a href={'https://britishspring.educare.school'} target={'_blank'}
                                           className={'portal-link'}>School Portal</a>}

                            {/*{button && (<a href={'https://britishspring.educare.school'} target={'_blank'}*/}
                            {/*               className={'portal-link'}>School Portal</a>)}*/}
                        </li>
                        <li className="nav-item social_link">
                            <a href="https://www.facebook.com/britishspringawka" target={'_blank'} style={{display: 'inline-block'}}>
                                <img src={Face} alt="face" className="img-fluid"/>
                            </a>
                        </li>
                        <li className="nav-item social_link">
                            <img src={Tweet} alt="face" className="img-fluid" />
                        </li>
                        <li className="nav-item social_link">
                            <a href="https://www.instagram.com/britishspringcollege" target={'_blank'} style={{display: 'inline-block'}}>
                                <img src={Insta} alt="face" className="img-fluid" />
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            {
                isMenuOverlayShown && (
                    <NavigationOverlay
                        onMouseEnterHandler={onMouseEnterHandler}
                        onMouseLeaveHandler={onMouseLeaveHandler}>
                        {overlayContent}
                    </NavigationOverlay>
                )
            }
        </div>
    )
}

export default NavBar
