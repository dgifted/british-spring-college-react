import React from 'react';
import {useHistory} from "react-router-dom";
import Carousel from 'react-elastic-carousel';
import Group from '../Assets/Rectangle 846.png';
import Award from '../Assets/Rectangle 869.png';
import Award1 from '../Assets/Rectangle 8467.png';
import Group1 from "../Assets/Rectangle 8453.png";
import Group2 from "../Assets/Rectangle 8454.png";
import Zoom from "../Assets/Rectangle 467.png";
import Zoom1 from "../Assets/Rectangle 8468.png";
import Zoom2 from "../Assets/Rectangle 8469.png";
import Group3 from "../Assets/Rectangle 8455.png";
import Button from 'react-bootstrap/Button';
import Micro from "../Assets/Microsoft_logo_(2012).svg"
import Micro1 from "../Assets/aisa-logo-blk.svg"
import Micro2 from "../Assets/alliance f.svg"
import Micro3 from "../Assets/cobis.svg"
import Micro4 from "../Assets/Layer 1 copy.svg"
import Micro5 from "../Assets/Image 12.png"
import Micro6 from "../Assets/Image 4.png"
import Item from "./Item";
import "./Join.css"


function Join() {
    const history = useHistory();

    return (
        <>
            <div className="container-lg ">
                <section>
                    <span className="Name-head mb-5">
                        <h1>
                        Awards & Recognition  
                        </h1>
                        <center><hr></hr></center>
                    </span>

                </section>
                <Carousel itemsToShow={3} className="hider">
                    <Item>
                        <img className="img img-fluid" src={Award1} alt="pro"/>
                        <div className="num-cont">
                            <h6>1st Position and Best Girl in Junior Science, National Olympiad 2017/2018</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img img-fluid" src={Group} alt="pro"/>
                        <div className="num-cont">
                            <h6>Overall Best School 2018 Olympiad Competition</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img img-fluid" src={Award} alt="pro"/>
                        <div className="num-cont">
                            <h6>Best in Senior Informatics (National Olympiad 2018/2019)</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img img-fluid" src={Zoom} alt="pro"/>
                        <div className="num-cont">
                            <h6>TOP SCHOOL IN NIGERIA 2016 by the British Council.</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img img-fluid" src={Zoom1} alt="pro"/>
                        <div className="num-cont">
                            <h6>2nd Position in Junior Science 2017/2018</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img img-fluid" src={Zoom2} alt="pro"/>
                        <div className="num-cont">
                            <h6>Olympiads Awards as the Best school in Junior Sciences 2015-2018</h6>
                        </div>
                    </Item>
                </Carousel>

                <Carousel itemsToShow={1} className="hiding">
                    <Item>
                        <img className="img " src={Award1} alt="pro"/>
                        <div className="num-cont">
                            <h6>1st Position and Best Girl in Junior Science, National Olympiad 2017/2018</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img " src={Group} alt="pro"/>
                        <div className="num-cont">
                            <h6>Overall Best School 2018 Olympiad Competition</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img " src={Award} alt="pro"/>
                        <div className="num-cont">
                            <h6>Best in Senior Informatics (National Olympiad 2018/2019)</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img " src={Zoom} alt="pro"/>
                        <div className="num-cont">
                            <h6>TOP SCHOOL IN NIGERIA 2016 by the British Council.</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img " src={Zoom1} alt="pro"/>
                        <div className="num-cont">
                            <h6>2nd Position in Junior Science 2017/2018</h6>
                        </div>
                    </Item>
                    <Item>
                        <img className="img " src={Zoom2} alt="pro"/>
                        <div className="num-cont">
                            <h6>Olympiads Awards as the Best school in Junior Sciences 2015-2018</h6>
                        </div>
                    </Item>
                </Carousel>
            </div>

            <section>
                <div className="container-lg  smooth">
                    <section>
                    <span className="Name-head mb-5">
                        <h1>
                            Join Us
                        </h1>
                        <center><hr></hr></center>
                    </span>
                    </section>
                    <div className="row mb-5">
                        <div className="col-md-4 col-lg-4 col-sm-12 col-xs-12  t-tour">
                            <img src={Group1} alt="stairs " className="img-fluid"/>
                            {/*<Button variant="primary" className="click1" size="lg" active>Inquire</Button>{' '}*/}
                            <a href="https://britishspring.educare.school/enquiry" target={'_blank'} className={'click1'}
                            style={{width: '100%', borderRadius: '5px'}}>Inquire</a>
                        </div>
                        <div className="col-md-4 col-lg-4 col-sm-12 col-xs-12 t-tour">
                            <img src={Group2} alt="stairs" className="img-fluid"/>
                            <a href="https://britishspring.educare.school/admission-form" target={'_blank'} className={'click1'}
                               style={{width: '100%', borderRadius: '5px'}}>Apply</a>

                        </div>
                        <div className="col-md-4 col-lg-4 col-sm-12 col-xs-12 t-tour">
                            <img src={Group3} alt="stairs" className="img-fluid"/>
                            <a href="https://britishspring.educare.school/enquiry" target={'_blank'} className={'click1'}
                               style={{width: '100%', borderRadius: '5px'}}>Take a tour</a>
                        </div>

                    </div>
                </div>


                {/*<div className="container-lg center ">*/}
                {/*    <div className="sponsors">*/}
                {/*        <img src={Micro} alt="mic" className="img-fluid  micro-7"/>*/}
                {/*        <img src={Micro6} alt="mic" className="img-fluid micro-7"/>*/}
                {/*        <img src={Micro5} alt="mic" className="img-fluid micro-7"/>*/}
                {/*        <img src={Micro3} alt="mic" className="img-fluid micro-7"/>*/}
                {/*        <img src={Micro4} alt="mic" className="img-fluid micro-7"/>*/}
                {/*        <img src={Micro1} alt="mic" className="img-fluid micro-7"/>*/}
                {/*        <img src={Micro2} alt="mic" className="img-fluid micro-7"/>*/}
                {/*    </div>*/}
                {/*</div>*/}


            </section>
        </>

    )
}

export default Join
