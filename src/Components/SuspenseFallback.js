import Spinner from "react-bootstrap/Spinner";

const SuspenseFallback = () => {
    return (
        <div style={{
            display: 'flex',
            position: 'fixed',
            top: '0',
            left: '0',
            flexDirection: 'column',
            width: '100vw',
            height: '100vh',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: '1000',
            backgroundColor: 'white'
        }}>
            <Spinner animation={'border'} size={'lg'} variant={'secondary'}/>
        </div>
    );
};

export default SuspenseFallback;
