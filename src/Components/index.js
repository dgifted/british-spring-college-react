import React from 'react';
import CarSlider from './CarSlider';
import Awards from './Awards';
import Join from './Join';
import MoveToTop from "./MoveToTop";


function index() {

    return (
        <>
            <CarSlider/>
            <Awards/>
            <Join/>

            <MoveToTop/>
        </>
    )
}

export default index
