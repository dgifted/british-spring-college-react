import React from 'react';
import "./Awards.css";
import Cape from "../Assets/ClipartKey_527556.png";
import Cert from "../Assets/certificate-degree.svg";
import Grade from "../Assets/grade (1).svg";
import Awe from "../Assets/Icon awesome-graduation-cap.svg";
import Icon from "../Assets/Icon awesome-school.svg";
import Social from "../Assets/social-media.svg";
import XMLID from "../Assets/XMLID_307_.svg";

function Awards() {
    return (
        <>
            <section >
                <div className="container-lg  ">
                    <section>
                        <span className="Name-head">
                            <h1>  BCS in Numbers   </h1>
                            <center><hr></hr></center>
                        </span>
                    </section>

                    <div className='row no-gutters'>
                        <div className="col-lg-4 col-md-4 col-sm-12">
                            <div className="graduate">
                                <div className="grad1">
                                    <div className="grad3">
                                        <h3>Students participating in Extra Curricular Activities</h3>
                                        <h1>98%</h1>
                                        <img src={Cape} alt="cape" className="img-fluid "></img>
                                    </div>
                                </div>
                                <div className="grad1">
                                    <div className="grad3">
                                        <h3>Enrolment since inception</h3>
                                        <h1>15,328</h1>
                                        <img src={XMLID} alt="cape" className="img-fluid "></img>
                                    </div>
                                </div>
                                <div className="grad1">
                                    <div className="grad3">
                                        <h3>Higher External Exam Scores Than National Average</h3>
                                        <h1>50%</h1>
                                        <img src={Grade} alt="cape" className="img-fluid "></img>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-12">
                            <div className="graduate">
                                <div className="grad2">
                                    <div className="grad13">
                                        <img src={Icon} alt="cape" className="img-fluid "></img>
                                        <h1>2</h1>
                                        <h3>Certified Schools</h3>
                                    </div>
                                </div>
                                <div className="grad2">
                                    <div className="grad13">
                                        <img src={Awe} alt="cape" className="img-fluid "></img>
                                        <h1>98%</h1>
                                        <h3>Pass Rate to Universities</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-12">
                            <div className="graduate">
                                <div className="grad1">
                                    <div className="grad3">
                                        <img src={Social} alt="cape" className="img-fluid "></img>
                                        <h1>98%</h1>
                                        <h3>Current students recommends us</h3>
                                    </div>
                                </div>
                                <div className="grad1">
                                    <div className="grad3">
                                        <h1>100%</h1>
                                        <h3>Satisfied Parents</h3>
                                    </div>
                                </div>
                                <div className="grad1">
                                    <div className="grad3">
                                        <img src={Cert} alt="cape" className="img-fluid "></img>
                                        <h1>90%</h1>
                                        <h3>Teachers with Advanced Degrees</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>







        </>
    )
}

export default Awards
