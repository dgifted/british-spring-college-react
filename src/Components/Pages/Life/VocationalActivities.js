import MoveToTop from "../../MoveToTop";
import HeaderVocational from "../../../Assets/images/banner_23.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchVocationalActivities = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/life/vocational-activities`);
    return res.json();
};

const VocationalActivities = () => {
    const location = useLocation();
    const {data, status} = useQuery('vocActivities', fetchVocationalActivities);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                The school has keen drive for vocation activities as a proactive form of developing our
                students and supporting our society with skillful youths. Our vocational activities
                include catering craft, music and dance club, animal husbandry, fine and applied arts,
                data processing, building/technical drawing, etc.
            </p>
        </>
    );

    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderVocational}
                           navLinks={NAVLINKS.life}
                           parentLinkText={'Life at BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.life)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default VocationalActivities
