import MoveToTop from "../../MoveToTop";
import React from "react";
import PageContainer from "../../../UI/PageContainer";
import HeaderFood from "../../../Assets/images/banner_21.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchFoodNut = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/life/food-nutrition`);
    return res.json();
};

const FoodNutrition = () => {
    const location = useLocation();
    const {data, status} = useQuery('foodNut', fetchFoodNut);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                British Spring College has a special notch for administration of food and nutrition with
                a world-class kitchen and bakery that offer our students the best.
            </p>
        </>
    );

    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderFood}
                           navLinks={NAVLINKS.life}
                           parentLinkText={'Life at BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.life)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default FoodNutrition;
