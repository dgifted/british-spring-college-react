import MoveToTop from "../../MoveToTop";
import PageContainer from "../../../UI/PageContainer";
import HeaderCultural from "../../../Assets/images/banner_19.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import {useLocation} from "react-router-dom";
import React from "react";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchCultural = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/life/cultural-activities`);
    return res.json();
};

const CulturalActivities = () => {
    const location = useLocation();
    const {data, status} = useQuery('cultural', fetchCultural);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                In the contemporary age of hybridity, our school is ever conscious of producing students
                that are of cultural blend.
            </p>
        </>
    );

    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderCultural}
                           navLinks={NAVLINKS.life}
                           parentLinkText={'Life at BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.life)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default CulturalActivities;
