import React from "react";
import MoveToTop from "../../MoveToTop";
import PageContainer from "../../../UI/PageContainer";
import HeaderMusic from "../../../Assets/images/banner_20.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchMusic = async () => {
    const res = await fetch(`${baseUrl}/pages/life/music`);
    return res.json();
};


const Music = () => {
    const location = useLocation();
    const {data, status} = useQuery('music', fetchMusic);

    const initContent = (
        <p style={paraLinesStyle}>
            We promote music in our school as a potential for child development.
            We also have an active school choir as we know that music is the bridge
            between divinity and humanity and the medicine of the soul.
        </p>
    );

    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderMusic}
                           navLinks={NAVLINKS.life}
                           parentLinkText={'Life at BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.life)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Music;
