import MoveToTop from "../../MoveToTop";
import HeaderSport from "../../../Assets/images/banner_22.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchSports = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/life/sports`);
    return res.json();
};

const Sports = () => {
    const location = useLocation();
    const {data, status} = useQuery('sports', fetchSports);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                As a student of British Spring College, you have many options for exercise
                and for keeping bodily fitness. We hold internal indoor and outdoor sporting
                activities on weekly basis. We have various standard sports facilities which
                enable our students to participate fully in the games of football, basketball,
                table tennis, lawn tennis, badminton, volleyball, etc.
            </p>
        </>
    );

    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderSport}
                           navLinks={NAVLINKS.life}
                           parentLinkText={'Life at BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.life)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Sports;
