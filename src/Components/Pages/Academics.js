import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';
import {motion} from 'framer-motion';

import {PageTransition, PageVariants} from "../../Utils";
import SuspenseFallback from "../SuspenseFallback";
import MoveToTop from "../MoveToTop";

const Facilities = React.lazy(() => import('./Academics/Facilities'));
const Curriculum = React.lazy(() => import('./Academics/Curriculum'));
const Library = React.lazy(() => import('./Academics/Library'));
const Methods = React.lazy(() => import('./Academics/Methods'));
const NotFound = React.lazy(() => import('./NotFound'));

const Academics = () => {
    const location = useLocation();

    return (
        <motion.div initial={'initial'}
                    animate={'in'}
                    exit={'out'}
                    variant={PageVariants}
                    transition={PageTransition}>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Route path={'/academics'} exact>
                        <Redirect to={'/academics/academic-facilities'}/>
                    </Route>
                    <Route path={'/academics/academic-facilities'} exact>
                        <Facilities/>
                    </Route>
                    <Route path={'/academics/curriculum'} exact>
                        <Curriculum/>
                    </Route>
                    <Route path={'/academics/library'} exact>
                        <Library/>
                    </Route>
                    <Route path={'/academics/academic-methods'} exact>
                        <Methods/>
                    </Route>

                    <Route path={'*'}>
                        <NotFound/>
                    </Route>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </motion.div>
    );
};
export default Academics;
