import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';
import {motion} from 'framer-motion';

import {PageTransition, PageVariants} from "../../Utils";
import SuspenseFallback from "../SuspenseFallback";

const CulturalActivities = React.lazy(() => import('./Life/CulturalActivities'));
const Music = React.lazy(() => import('./Life/Music'));
const FoodNutrition = React.lazy(() => import('./Life/FoodNutrition'));
const Sports = React.lazy(() => import('./Life/Sports'));
const VocationalActivities = React.lazy(() => import('./Life/VocationalActivities'));
const NotFound = React.lazy(() => import('./NotFound'));



const Life = () => {
    const location = useLocation();

    return (
        <motion.div initial={'initial'}
                    animate={'in'}
                    exit={'out'}
                    variant={PageVariants}
                    transition={PageTransition}>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Route path={'/life'} exact>
                        <Redirect to={'/life/cultural-activities'}/>
                    </Route>
                    <Route path={'/life/cultural-activities'}>
                       <CulturalActivities/>
                    </Route>
                    <Route path={'/life/music'}>
                       <Music/>
                    </Route>
                    <Route path={'/life/food-nutrition'}>
                       <FoodNutrition/>
                    </Route>
                    <Route path={'/life/sports'}>
                        <Sports/>
                    </Route>
                    <Route path={'/life/vocational-activities'}>
                        <VocationalActivities/>
                    </Route>
                    <Route path={'*'}>
                        <NotFound/>
                    </Route>
                </Switch>
            </Suspense>
        </motion.div>
    );
};
export default Life;
