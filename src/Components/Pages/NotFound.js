import {PageTransition, PageVariants} from "../../Utils";
import {motion} from 'framer-motion';
import {Link} from "react-router-dom";
import classes from './NotFound.module.css';

// const pageStyle = {position: 'absolute'};

const NotFound = () => {
    return (
        <motion.div initial={'initial'}
                    className={classes['not-found-page__wrapper']}
                    animate={'in'}
                    exit={'out'}
                    variant={PageVariants}
                    transition={PageTransition}>
            <section className={classes['not-found-page__content']}>
                <h2>Page not found.</h2>
                <p className={'text-center'}>
                    <Link to={'/'} className={'btn btn-primary btn-lg'}>Back to Home</Link>
                </p>
            </section>

        </motion.div>
    );
};
export default NotFound;
