import MoveToTop from "../../MoveToTop";
import React from "react";
import HeaderCurriculum from "../../../Assets/images/banner_15.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const emListsStyle = {
    textAlign: 'left',
    paddingLeft: '2rem',
};

const emListsItemStyle = {
    marginTop: '1rem',
    fontSize: '20px',
    fontFamily: 'Gill'
};

const fetchCurriculum = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/academics/curriculum`);
    return res.json();
};

const Curriculum = () => {
    const location = useLocation();
    const {data, status} = useQuery('curriculum', fetchCurriculum);
    console.log('data', data)
    const initContent = (
        <>
            <p style={paraLinesStyle}>
                The curriculum of British Spring College is designed to take care of the interest
                of students from all parts of the world. The school operates basically, the Nigerian
                and the British National curriculum but is also patterned to suit students from the
                United States of America and other parts of the world. We are poised to be strictly
                international in entirety: topics, scope of content elements, delivery methods,
                objectives, attainment targets, assessment method and resources. <br/>Our terms of
                reference are:
            </p>
            <ol style={emListsStyle}>
                <li style={emListsItemStyle}>JSSCE Curriculum</li>
                <li style={emListsItemStyle}>Checkpoint Syllabus</li>
                <li style={emListsItemStyle}>SSSCE O’ Level</li>
                <li style={emListsItemStyle}>JAMB Syllabus</li>
                <li style={emListsItemStyle}>IGCSE Cambridge</li>
                <li style={emListsItemStyle}>SAT</li>
                <li style={emListsItemStyle}>TOEFL</li>
                <li style={emListsItemStyle}>IELTS</li>
            </ol>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }
    return (
        <>
            <PageContainer headerImage={HeaderCurriculum}
                           navLinks={NAVLINKS.academics}
                           parentLinkText={'Academics'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.academics)}>
                {pageContent}
            </PageContainer>
            <MoveToTop/>
        </>
    );
};

export default Curriculum;
