import MoveToTop from "../../MoveToTop";
import React from "react";
import HeaderLibrary from "../../../Assets/images/banner_16.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchLibrary = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/academics/library`);
    return res.json();
};

const Library = () => {
    const location = useLocation();
    const {data, status} = useQuery('library', fetchLibrary);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                The newly constructed British Spring College Library is stocked with myriads
                of modern and up-to-date academic and vocational collections.These are geared
                towards promoting learning, teaching and research among our students and teachers.
            </p>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }
    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderLibrary}
                           navLinks={NAVLINKS.academics}
                           parentLinkText={'Academics'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.academics)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Library;
