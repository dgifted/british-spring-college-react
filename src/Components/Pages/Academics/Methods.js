import MoveToTop from "../../MoveToTop";
import HeaderMethods from "../../../Assets/images/banner_17.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};
const fetchMethods = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/academics/academic-methods`);
    return res.json();
};

const Methods = () => {
    const location = useLocation();
    const {data, status} = useQuery('methods', fetchMethods);

    const initContent = (
        <>
            <p style={paraLinesStyle}>In view of academic methods, our ever task-based methodology of
                learning and
                teaching has sustained our students in skills acquisition and development.
                More so, our technological approach to learning proves a sustainable tool in
                support of our students especially in the wake of the global pandemic - COVID-19,
                when learning has gone technological and online. </p>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderMethods}
                           navLinks={NAVLINKS.academics}
                           parentLinkText={'Academics'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.academics)}>
                {pageContent}
            </PageContainer>
            <MoveToTop/>
        </>
    );
};

export default Methods;
