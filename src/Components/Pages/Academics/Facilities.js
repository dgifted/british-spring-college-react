import React from "react";
import MoveToTop from "../../MoveToTop";
import HeaderFacilities from "../../../Assets/images/banner_14.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};

const fetchFacilities = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/academics/academic-facilities`);
    return res.json();
};

const Facilities = () => {
    const location = useLocation();
    const {data, status} = useQuery('facilities', fetchFacilities);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                We use technology as a tool in teaching, learning, administration and research throughout
                the classes in British Spring College. We have spent substantial amount of money in
                equipping all laboratories and specialist rooms, networking, internet access, peripherals
                and mobile computer labs for effective classroom teaching, curriculum development and teacher training.
                A major feature of our curriculum is the extensive use of Information and Communication
                Technology skills and equipment (interactive whiteboards, televisions and powerpoint).
                We also have well lit and air-conditioned classrooms.
            </p>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }
    pageContent = initContent;

    if (status === 'success' && data?.data?.content) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}} />;
    }


    return (
        <>
            <PageContainer headerImage={HeaderFacilities}
                           navLinks={NAVLINKS.academics}
                           parentLinkText={'Academics'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.academics)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Facilities;
