import React, {Suspense} from "react";
import {NavLink, Redirect, Route, Switch, useLocation} from 'react-router-dom';
import {motion} from 'framer-motion';

import NAVLINKS from "../../NavLinksData";
import "./About.css"
import {extractActiveLink, PageVariants, PageTransition} from "../../Utils";
import MoveToTop from "../MoveToTop";
import SuspenseFallback from "../SuspenseFallback";

const DirectorWelcome = React.lazy(() => import('./About/DirectorWelcome'));
const MissionVision = React.lazy(() => import('./About/MissionVision'));
const Calendar = React.lazy(() => import('./About/Calendar'));
const Policies = React.lazy(() => import('./About/Policies'));
const VirtualTour = React.lazy(() => import('./About/VirtualTour'));
const Achievements = React.lazy(() => import('./About/Achievements'));
const ContactDirections = React.lazy(() => import('./About/ContactDirections'));
const SchoolNews = React.lazy(() => import('./About/SchoolNews'));
const NewsDetail = React.lazy(() => import('./About/NewsDetail'));
const NotFound = React.lazy(() => import('./NotFound'));


const pageNavLinks = <ul>
    {NAVLINKS.about.map((link, index) =>
        (<li key={index}>
            <NavLink to={`/about/${link.path}`} activeClassName={'active-link'}>{link.text}</NavLink>
        </li>))}
</ul>;

function About() {
    const location = useLocation();

    const emListsStyle = {
        textAlign: 'left',
        paddingLeft: '2rem',
    };

    const emListsItemStyle = {
        marginTop: '1rem',
        fontSize: '20px',
        fontFamily: 'Gill'
    };

    return (
        <motion.div initial={'initial'}
                    animate={'in'}
                    exit={'out'}
                    variant={PageVariants}
                    transition={PageTransition}>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Route path={'/about'} exact>
                        <Redirect to={'/about/director-welcome'}/>
                    </Route>
                    <Route path={'/about/director-welcome'} exact>
                        <DirectorWelcome pageNavLinks={pageNavLinks}/>
                    </Route>
                    <Route path={'/about/story-vision-mission'} exact>
                        <MissionVision />
                    </Route>
                    <Route path={'/about/calendar'} exact>
                        <Calendar/>
                    </Route>
                    <Route path={'/about/school-policies'}>
                        <Policies/>
                    </Route>
                    <Route path={'/about/virtual-tour'} exact>
                        <VirtualTour/>
                    </Route>
                    <Route path={'/about/achievements'} exact>
                        <Achievements/>
                    </Route>
                    <Route path={'/about/school-news'} exact>
                        <SchoolNews/>
                    </Route>
                    <Route path={'/about/school-news/:id'} exact>
                        <NewsDetail/>
                    </Route>
                    <Route path={'/about/contact-directions'} exact>
                        <ContactDirections/>
                    </Route>
                    <Route path={'*'}>
                        <NotFound/>
                    </Route>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </motion.div>

    )
}

export default About
