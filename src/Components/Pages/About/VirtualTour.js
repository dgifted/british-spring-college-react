import HeaderTour from "../../../Assets/images/banner_4.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";
import {useQuery} from "react-query";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";

const fetchTour = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/about/virtual-tour`);
    return res.json();
};

const VirtualTour = () => {
    const location = useLocation();
    const {data, status} = useQuery('virtualTour', fetchTour);
    console.log('DATA ', data);

    const initContent = (<>
        <p>Virtual tour page.</p>
    </>);
    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    if (status === 'error') {
        pageContent = initContent;
    }

    if (status === 'success' && data?.data?.content === null) {
        pageContent = initContent;
    }

    if (status === 'success' && data?.data?.content) {
        pageContent = <article dangerouslySetInnerHTML={{__html:data.data?.content}} />
    }

    return (
        <>
            <PageContainer headerImage={HeaderTour}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default VirtualTour;
