import Header from "../../../Assets/images/Group 3.png";
import Breadcrumb from "../../Breadcrumb";
import {extractActiveLink} from "../../../Utils";
import NAVLINKS from "../../../NavLinksData";
import Direct from "../../../Assets/images/Image 10.png";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";

const DirectorWelcome = (props) => {
    const location = useLocation();

    return (
        <>
            <section>
                <div className="header-img">
                    <img src={Header} alt="Header" className="img-fluid" />
                </div>
            </section>

            <section>
                <div className="container-md">
                    <div className="note-lists">
                        {props.pageNavLinks}
                    </div>
                    <div className="bread-crumbs">
                        <div className={'bold'}>You are here:</div>
                        <Breadcrumb parentLinkText={'About'}
                                    activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)} />
                    </div>
                </div>
            </section>

            <section className="container-md">
                <div className="comments">
                    <div className="intro">
                        <small>About BSC</small>
                        <header>Director’s Welcome</header>
                    </div>
                    <div className="row create">
                        <div className="col-lg-3 col-xl-3 col-md-3 col-sm-12 make">
                            <img src={Direct} alt="Director" className="img-fluid" />
                        </div>
                        <div className="col-lg-9 col-xl-9 col-md-9 col-sm-12 make">
                            <div className="speech">
                                <p>Welcome to British Spring College‘s website. Thank you for taking time to
                                    Learn
                                    about us. We hope the links provide a glimpse of our brand of
                                    excellence.
                                    Our
                                    motto is “inspiring the best in your child” as we generally work hard to
                                    provide
                                    the students with a sound and solid education while at the same time
                                    developing
                                    the total child to face the challenges of the evolving world of the
                                    twenty-first
                                    century. This totality encompasses the child’s physical, social,
                                    spiritual,
                                    academic and every variegated aspect of development. Our vision is
                                    therefore
                                    anchored on the resolution to raise a generation of students with the
                                    right
                                    tools, skills, moral and mental abilities to excel in life and
                                    contribute
                                    meaningfully to the development of our modern society.</p>
                                <p>We strongly believe that our school offers a unique educational
                                    experience
                                    for
                                    all students by providing a hybrid of the British and the Nigerian
                                    curricula
                                    to
                                    achieve a genuine international community and perspective for all our
                                    students.
                                    We sincerely thank you for taking out time to visit our website and we
                                    look
                                    forward to your visits to our school. Please do not hesitate to contact
                                    us
                                    if
                                    you need any information because we believe that British Spring College
                                    is
                                    the
                                    right place for your child.</p>
                                <strong>Happiness and success are the two things that most parents put at
                                    the
                                    top of
                                    their wish list for their children. Here, we prioritise happiness. If
                                    our
                                    pupils
                                    – and our staff – are happy, success will follow.</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <MoveToTop/>
        </>
    );
};

export default DirectorWelcome;
