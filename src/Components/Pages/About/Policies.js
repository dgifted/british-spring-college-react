import React, {useEffect, useState} from "react";

import HeaderPolicies from "../../../Assets/images/banner_7.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";
import {useQuery} from 'react-query';
import fetchData from "../../../utils/fetch-data";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";

const emListsStyle = {
    textAlign: 'left',
    paddingLeft: '2rem',
};

const emListsItemStyle = {
    marginTop: '1rem',
    fontSize: '20px',
    fontFamily: 'Gill'
};

const fetchSchoolPolicies = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/about/school-policies`);
    return res.json();
}

const Policies = () => {
    const location = useLocation();
    const {data, status} = useQuery('policies', fetchSchoolPolicies);

    const initContent = (
        <>
            <h4 style={{fontFamily: 'Gill'}}>We have adopted a lot of useful policies for smooth
                administration and support to our students. The policies include:</h4>
            <ul style={emListsStyle}>
                <li style={emListsItemStyle}>Anti-bullying policy</li>
                <li style={emListsItemStyle}>Behaviour</li>
                <li style={emListsItemStyle}>Child protection</li>
                <li style={emListsItemStyle}>Safeguard</li>
            </ul>
        </>
    );
    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    if (status === 'error') {
        pageContent = initContent;
    }

    if (status === 'success' && data.data.content === null) {
        pageContent = initContent
    }

    if (status === 'success' && data.data.content) {
        pageContent = (
            <>
                <article dangerouslySetInnerHTML={{__html: data.data.content}}/>
            </>
        );
    }

    return (
        <>
            <PageContainer headerImage={HeaderPolicies}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Policies;
