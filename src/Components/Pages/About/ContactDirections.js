import React from "react";
import MoveToTop from "../../MoveToTop";
import HeaderContact from "../../../Assets/images/banner_6.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {useQuery} from "react-query";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";

const fetchContactDirection = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/about/contact-directions`);
    return res.json();
};

const ContactDirections = () => {
    const location = useLocation();
    const {data, status} = useQuery('contactDirection', fetchContactDirection);
    const initContent = (
        <>
            <Card>
                <Card.Body style={{border: 'none', margin: '0', padding: '3rem'}}>
                    <Row>
                        <Col sm={12} md={6}>
                            <h4>Awka</h4>
                            <div style={{lineHeight: '200%'}}>
                                By Ring Road:1, British Spring Estate Road, Nkwelle,Awka, Anambra State,
                                Nigeria.
                            </div>
                        </Col>
                        <Col sm={12} md={6}>
                            <h4>Onitsha</h4>
                            <div style={{lineHeight: '200%'}}>
                                Springfield Academy, 30 New Nkisi Road, GRA Onitsha, Anambra state.
                            </div>
                        </Col>
                    </Row>
                    <Row className={'mt-5'}>
                        <Col sm={12} md={{span: 6, offset: 3}}>
                            <div><b style={{fontSize: '1.5rem'}}>+2347032861442</b></div>
                            <div><b style={{fontSize: '1.5rem'}}>+2348037588614</b></div>
                            <div><b style={{fontSize: '1.5rem'}}>+2347034948445</b></div>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </>
    )
    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>
    } else {
        pageContent = initContent;

        if (status === 'success' && data?.data?.content !== null) {
            pageContent = <article dangerouslySetInnerHTML={{__html:  data?.data?.content}}/>
        }
    }

    return (
        <>
            <PageContainer headerImage={HeaderContact}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default ContactDirections;
