import React, {useEffect, useState} from "react";
import {useLocation, useParams} from 'react-router-dom';
import HeaderAchievements from "../../../Assets/images/banner_5.png";
import NAVLINKS from "../../../NavLinksData";
import NewsLayout from "../../../UI/NewsLayout";
import PageContainer from "../../../UI/PageContainer";
import {backEndUrl, baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";
import classes from './NewsDetail.module.css';
import {extractActiveLink} from "../../../Utils";


const NewsDetail = () => {
    const location = useLocation();
    const params = useParams();
    const [news, setNews] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    const fetchNewsItem = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/news/${params.id}`);
        return res.json();
    };


    useEffect(() => {
        if (!isLoading) {
            setIsLoading(true);
        }

        fetchNewsItem()
            .then(res => {
                console.log('Res => ', res);
                setIsLoading(false);
                setNews(res.data)
            })
            .catch(e => {
                console.log('Error => ', e);
                setIsLoading(false);
                setError(e.toString())
            });

        return () => setError(null);
    }, [params.id]);

    let pageContent;
    if (isLoading) {
        pageContent = <SuspenseFallback/>;
    }
    if (!isLoading && !error && !news) {
        pageContent = <div className={'text-center my-5 p-5'}>
            News post not found.
        </div>;
    }

    if (error) {
        pageContent = <div className={'text-center my-5 p-5'}>
            Error getting news detail. Please refresh page.
        </div>
    }

    if (!isLoading && !error && news) {
        let newsBanner = `https://picsum.photos/id/1023/900/300`
        if (news.banner) {
            newsBanner = `${process.env.REACT_APP_SERVER_URL}/storage/${news.banner}`;
        }
        pageContent = (
            <>
                <div className={classes['thumbnail']} style={{
                    background: `url(${newsBanner}) center/cover no-repeat padding-box`
                }}/>
                <h2 className={`${classes['title']}`}>{news.title}</h2>
                <div className={'d-flex justify-content-start'}>
                    <span></span>
                </div>
                <article className={classes['content']} dangerouslySetInnerHTML={{__html: news?.content}}/>
                <hr/>
            </>
        );
    }

    return (
        <>
            <PageContainer headerImage={HeaderAchievements}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                <NewsLayout>
                    {pageContent}
                </NewsLayout>
            </PageContainer>
        </>
    );
};

export default NewsDetail;
