import HeaderAchievements from "../../../Assets/images/banner_5.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import {useLocation} from "react-router-dom";
import {useQuery} from "react-query";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";
import MoveToTop from "../../MoveToTop";

const emListsStyle = {
    textAlign: 'left',
    paddingLeft: '2rem',
};

const emListsItemStyle = {
    marginTop: '1rem',
    fontSize: '20px',
    fontFamily: 'Gill'
};
const fetchAchievements = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/about/achievements`);
    return res.json();
};

const Achievements = () => {
    const location = useLocation();
    const {data, status} = useQuery('achievements', fetchAchievements);

    const initContent = (
        <>
            <h4 style={{fontFamily: 'Gill', padding: '0', margin: '0'}}>British Spring College has
                recorded a lot of achievements in academic, administrative and extra-curricular
                affairs:</h4>
            <ul style={emListsStyle}>
                <li style={emListsItemStyle}>The Best School of the Year in Anambra State in 2020</li>
                <li style={emListsItemStyle}>The Best Proprietor of the Year in Anambra State in 2020
                </li>
                <li style={emListsItemStyle}>Production of international students at tertiary level
                    across the globe
                </li>
                <li style={emListsItemStyle}>The Second-Best Secondary School in Nigeria in 2020</li>
            </ul>
        </>
    );
    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }
    if (status === 'error') {
        pageContent = initContent;
    }

    if (status === 'success' && data?.data?.content === null) {
        pageContent = initContent;
    }

    if (status === 'success' && data?.data?.content) {
        pageContent = <article dangerouslySetInnerHTML={{__html:data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer headerImage={HeaderAchievements}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Achievements;
