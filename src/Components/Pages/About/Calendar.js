import HeaderCalender from "../../../Assets/images/banner_3.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import PageContainer from "../../../UI/PageContainer";
import React from "react";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";

const Calendar = () => {
    const location = useLocation();
    const calendarEvents = [
        {
            id: 1,
            title: 'Resumption of our old students',
            date: '2021-09-11'
        },
        {
            id: 2,
            title: 'Final vacation for Year 12 students',
            date: '2021-10-09'
        },
        {
            id: 3,
            title: 'Extension classes for the outgoing Year 12 students',
            date: '2021-10-08'
        },
        {
            id: 4,
            title: 'Resumption of new students',
            date: '2021-10-10'
        }
    ];

    return (
        <>
            <PageContainer headerImage={HeaderCalender}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                <Row>
                    <Col sm={12} md={8}>
                        <FullCalendar
                            plugins={[dayGridPlugin]}
                            initialView="dayGridMonth"
                            events={calendarEvents}
                        />
                    </Col>
                    <Col sm={12} md={4} className={'text-left d-flex flex-column justify-content-center'}>
                        {calendarEvents.map((event, idx) => (
                            <div key={idx}>
                                <span className={'calendar-date'}>{event.date}</span>
                                <span className={'calendar-title'}>{event.title}</span>
                            </div>
                        ))}
                    </Col>
                </Row>
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Calendar;
