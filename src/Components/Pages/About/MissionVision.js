import HeaderMission from "../../../Assets/images/banner_1.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";

const MissionVision = () => {
    const location = useLocation();

    return (
        <>
            <PageContainer headerImage={HeaderMission}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                <p>British Spring College is located at Awka, Capital City of Anambra State on 30 Acres of land.
                    Awka is one of the oldest towns in the Eastern Region of Nigeria, known for their deep love
                    for
                    social, cultural and tourist ventures. The peaceful town is the Capital City of Anambra
                    State
                    with average of 500,000 habitants. The school is located at the outskirts of Nkwelle, a
                    community at the centre of Awka City. British Spring College was created as a private
                    international school to provide world-class education with the use of modern technologies,
                    catering for children aged 10 to 18. Our standards and expectations are high. We pride
                    ourselves
                    in giving every student the opportunity to excel, to fulfil their potential and become
                    highly
                    qualified, equipped fully for the demands and exciting challenges of life.</p>
                <p className={'py-3'}><strong>Over the years, we’ve been able to build a reputation as one of
                    the best independent High
                    schools and we are still the best.</strong></p>
                <p>Our vision is to raise a generation of students with the right tools, skills, moral and
                    mental
                    abilities to excel in life and contribute positively to the development of mankind. In
                    fulfilling our mission, we seek to achieve the following:</p>
                <ul className={'text-left'}>
                    <li className={'font-primary'}>Impart high quality education to students and develop
                        enquiring minds and a desire for knowledge.
                    </li>
                    <li className={'font-primary'}>Develop strong self-esteem and high personal expectation, and
                        acquire a set of spiritual and moral values.
                    </li>
                    <li className={'font-primary'}>Equip students with the tools to excel in life and skills to
                        manage changes.
                    </li>
                    <li className={'font-primary'}>Provide conducive environment for learning, and expose
                        students to the rich and diverse culture of the environment and Nigeria in general.
                    </li>
                    <li className={'font-primary'}>Impart leadership skills.</li>
                </ul>
            </PageContainer>

            <MoveToTop/>
        </>
    );
};


export default MissionVision;
