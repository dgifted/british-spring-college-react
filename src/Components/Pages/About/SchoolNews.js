import React from "react";
import PageContainer from "../../../UI/PageContainer";
import HeaderAchievements from "../../../Assets/images/banner_5.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import MoveToTop from "../../MoveToTop";
import {useLocation} from "react-router-dom";
import {useQuery} from "react-query";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";
import NewsLayout from "../../../UI/NewsLayout";
import NewsCard from "../../NewsCard";

const fetchNews = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/news`);
    return res.json();
};


const SchoolNews = () => {
    const location = useLocation();
    const {data, status} = useQuery('news', fetchNews);

    let pageContent;

    if (status === "loading") {
        pageContent = <SuspenseFallback/>;
    }

    if (status === "error") {
        pageContent = <div className={'my-5 p-5 text-center w-100'}>
            <h3>Could not get news at the moment. Please try again later.</h3>
        </div>;
    }

    if (status === "success" && data?.data?.length === 0) {
        pageContent = <div className={'my-5 p-5 text-center w-100'}>
            <h3>No news found in record.</h3>
        </div>;
    }

    if (status === "success" && data?.data?.length > 0) {
        pageContent = (
            <>
                {data?.data?.map(news => (
                    <NewsCard news={news} key={news.id}/>
                ))}
            </>
        );
    }

    return (
        <>
            <PageContainer headerImage={HeaderAchievements}
                           navLinks={NAVLINKS.about}
                           parentLinkText={'About BSC'}
                           activeLinkText={extractActiveLink(location.pathname, NAVLINKS.about)}>
                <NewsLayout>
                    {pageContent}
                </NewsLayout>
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default SchoolNews;
