import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation, useHistory} from 'react-router-dom';
import {motion} from 'framer-motion';

import {PageTransition, PageVariants} from "../../Utils";
import NotFound from "./NotFound";
// import Button from 'react-bootstrap/Button'
import SuspenseFallback from "../SuspenseFallback";
import Scholarship from "./Admission/Scholarship";

const Procedure = React.lazy(() => import('./Admission/Procedure'));
const TuitionFees = React.lazy(() => import('./Admission/TuitionFees'));
const Boarding = React.lazy(() => import('./Admission/Boarding'));
const FAQs = React.lazy(() => import('./Admission/FAQs'));


const Admission = () => {
    const location = useLocation();

    return (
        <motion.div initial={'initial'}
                    animate={'in'}
                    exit={'out'}
                    variant={PageVariants}
                    transition={PageTransition}>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Route path={'/admission'} exact>
                        <Redirect to={'/admission/admission-procedure'}/>
                    </Route>

                    <Route path={'/admission/admission-procedure'}>
                        <Procedure/>
                    </Route>
                    <Route path={'/admission/tuition-fees'}>
                        <TuitionFees/>
                    </Route>
                    <Route path={'/admission/boarding'}>
                        <Boarding/>
                    </Route>
                    <Route path={'/admission/scholarship'}>
                        <Scholarship/>
                    </Route>
                    <Route path={'/admission/faq'}>
                        <FAQs/>
                    </Route>
                    <Route path={'*'}>
                        <NotFound/>
                    </Route>
                </Switch>
            </Suspense>
        </motion.div>
    );
};
export default Admission;
