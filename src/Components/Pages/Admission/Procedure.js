import MoveToTop from "../../MoveToTop";
import React from "react";
import HeaderProcedure from "../../../Assets/images/banner_9.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {useQuery} from "react-query";
import {baseUrl} from "../../../utils/static-contents";
import SuspenseFallback from "../../SuspenseFallback";

const subHeadingStyle = {
    fontFamily: 'Gill',
    textAlign: 'left'
};
const emListsStyle = {
    textAlign: 'left',
    paddingLeft: '2rem',
};

const emListsItemStyle = {
    marginTop: '1rem',
    fontSize: '20px',
    fontFamily: 'Gill'
};

const fetchProcedure = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/admission/admission-procedure`);
    return res.json();
};

const Procedure = () => {
    const location = useLocation();
    const {data, status} = useQuery('procedure', fetchProcedure);

    const initContent = (
        <>
            <h4 style={subHeadingStyle}>Our admission procedure involves the following steps:</h4>
            <ol style={emListsStyle}>
                <li style={emListsItemStyle}>
                    Purchase of entrance examination form from designated centres or from the school
                    secretary.
                </li>
                <li style={emListsItemStyle}>Written entrance examination</li>
                <li style={emListsItemStyle}>Oral interview</li>
            </ol>
            <div className={'text-left mt-4'}>
                {/*<Button */}
                {/*style={{ backgroundColor: '#BA4A6E' }} */}
                {/*size={'lg'}*/}
                {/*    onClick={() => history.push('/admission/apply')}>Apply online</Button>*/}
                {/*<a href={'https://britishspring.educare.school/admission-form'} target={'_blank'} style={{backgroundColor: '#BA4A6E', padding: '10px 20px', color: 'white', borderRadius: '5px', fontSize: '1.3rem' }}>Apply online</a>*/}
                <button style={{
                    backgroundColor: '#BA4A6E',
                    padding: '10px 20px',
                    color: 'white',
                    borderRadius: '5px',
                    fontSize: '1.3rem',
                    border: 'none'
                }}>Application not open for now
                </button>
            </div>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }
    return (
        <>
            <PageContainer
                headerImage={HeaderProcedure}
                navLinks={NAVLINKS.admission}
                parentLinkText={'Admission'}
                activeLinkText={extractActiveLink(location.pathname, NAVLINKS.admission)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Procedure;
