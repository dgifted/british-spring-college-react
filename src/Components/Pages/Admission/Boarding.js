import MoveToTop from "../../MoveToTop";
import React from "react";
import HeaderBoarding from "../../../Assets/images/banner_12.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import classes from "../Admission.module.css";
import FacilityCard from "../../../UI/FacilityCard";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import hostelFacilities from "../../../data";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const facilities = hostelFacilities;

const fetchBoarding = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/admission/boarding`);
    return res.json();
};
const Boarding = () => {
    const location = useLocation();
    const {data, status} = useQuery('boarding', fetchBoarding);

    const initContent = (
        <>
            <p style={{lineHeight: '200%'}}>
                Boarding life enables students to develop self-reliance, confidence and sense of
                independence.
                It creates an atmosphere for students to appreciate the values of community and social
                life.
                We ensure we provide a suitable, friendly and relaxed environment with committed and
                caring staff
                for our students such that they need not miss home. The need for a boarding life is to
                enable
                students enjoy special attention and extensive educational curricular with sincere and
                warm care.
                This is cost effective in the cases of professional parents who hardly find time for
                life outside business.</p>

            <p style={{lineHeight: '200%'}}>We help provide that special care their children need while
                saving the discomfort of combining family and business life.</p>

            <p style={{lineHeight: '200%'}}>Our boarding facilities are fully air-conditioned rooms with
                toilet and a bathroom in each room,
                well built wardrobe for each student, modern dining hall. Proper health care is another
                thing
                that makes our boarding life so enjoyable because we have well trained and motivated
                medical personnel.</p>

            <div className={classes['facility-display']}>
                {facilities.map((facility, idx) => (
                    <FacilityCard facility={facility} key={idx}/>
                ))}
            </div>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html:  data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer
                headerImage={HeaderBoarding}
                navLinks={NAVLINKS.admission}
                parentLinkText={'Admission'}
                activeLinkText={extractActiveLink(location.pathname, NAVLINKS.admission)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Boarding;
