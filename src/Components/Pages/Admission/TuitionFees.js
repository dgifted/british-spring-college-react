import MoveToTop from "../../MoveToTop";
import React from "react";
import HeaderTuition from "../../../Assets/images/banner_10.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const fetchTuition = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/admission/tuition-fees`);
    return res.json();
};

const TuitionFees = () => {
    const location = useLocation();
    const {data, status} = useQuery('tuition', fetchTuition);

    const initContent = (
        <>
            <p className={'text-center'} style={{lineHeight: '200%'}}>Moderation is a major principle
                guiding fixture of tuition in the school.
                We charge moderate fees on termly basis despite ourhigh-tech facilities
                and world-class services.</p>
        </>
    );
    let pageContent;

    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer
                headerImage={HeaderTuition}
                navLinks={NAVLINKS.admission}
                parentLinkText={'Admission'}
                activeLinkText={extractActiveLink(location.pathname, NAVLINKS.admission)}>
                {pageContent}
            </PageContainer>
            <MoveToTop/>
        </>
    );
};

export default TuitionFees;
