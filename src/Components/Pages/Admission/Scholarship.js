import MoveToTop from "../../MoveToTop";
import React from "react";
import PageContainer from "../../../UI/PageContainer";
import HeaderScholarship from "../../../Assets/images/banner_11.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const paraLinesStyle = {
    lineHeight: '200%'
};
const emListsStyle = {
    textAlign: 'left',
    paddingLeft: '2rem',
};

const emListsItemStyle = {
    marginTop: '1rem',
    fontSize: '20px',
    fontFamily: 'Gill'
};

const fetchScholarship = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/admission/scholarship`);
    return res.json();
};

const Scholarship = () => {
    const location = useLocation();
    const {data, status} = useQuery('scholarship', fetchScholarship);

    const initContent = (
        <>
            <p style={paraLinesStyle}>
                The school runs an amazing scholarship scheme via its annual Mathematics Competition
                for Primary 5 & 6 pupils. The school welcomes application for the scholarship
                examination
                from eligible pupils round the country. The scholarship opportunity ranges from partial
                to
                full scholarship. Other monetary and non-monetary prizes are also given to successful
                students.
            </p>
            <p style={paraLinesStyle}>
                Students who seek admission into our school will have the opportunity to acquire a
                scholarship place. The population of British Spring scholarship students vary yearly; it
                is based mainly on the academic performance of the applicants. The scholarship scheme
                applies to tuition fees only. It is possible to achieve a full scholarship on tuition
                fees at British Spring, but performance must be exceptionally outstanding.

                The percentage deduction on tuition fees ranges from 15 to 75% based on the outcome of
                individuals’ academic assessments. Providing the scholarship opportunity in our school
                is a way of giving back to society and acknowledging our gifted learners regardless of
                gender or background. The scholarship examination is not different from the main Year 7
                entrance exam so the students who are eligible are selected from that group of
                applicants.

                For the 2018/2019 academic year, prospective students have already been offered a
                scholarship place, and these students are from various schools. Academic scholarships
                are awarded to the most outstanding boy and girl in the entrance examination. This
                covers fully the tuition and boarding fees.
            </p>
            <h3 style={{textAlign: 'left'}}>Requirements for Scholarships.</h3>
            <p>Candidates must bring the following to the examination hall:</p>
            <ol style={emListsStyle}>
                <li style={emListsItemStyle}>Pencil</li>
                <li style={emListsItemStyle}>Mathematical set</li>
            </ol>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer
                headerImage={HeaderScholarship}
                navLinks={NAVLINKS.admission}
                parentLinkText={'Admission'}
                activeLinkText={extractActiveLink(location.pathname, NAVLINKS.admission)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default Scholarship;
