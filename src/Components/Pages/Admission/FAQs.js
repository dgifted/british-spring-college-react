import React from "react";
import MoveToTop from "../../MoveToTop";
import HeaderFaq from "../../../Assets/images/banner_13.png";
import NAVLINKS from "../../../NavLinksData";
import {extractActiveLink} from "../../../Utils";
import PageContainer from "../../../UI/PageContainer";
import {useLocation} from "react-router-dom";
import {baseUrl} from "../../../utils/static-contents";
import {useQuery} from "react-query";
import SuspenseFallback from "../../SuspenseFallback";

const faqHeading = {
    color: '#BA4A6E',
    fontStyle: 'italic',
    textAlign: 'left'
};

const faqAnswer = {
    paddingLeft: '1rem',
    marginBottom: '2rem'
};

const fetchFaqs = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/pages/admission/faq`);
    return res.json();
};

const FAQs = () => {
    const location = useLocation();
    const {data, status} = useQuery('faqs', fetchFaqs);

    const initContent = (
        <>
            <h4 style={faqHeading}>Q: What makes your school different from other schools?</h4>
            <p style={faqAnswer}>A:</p>

            <h4 style={faqHeading}>Q: What is the position of your school in view of discipline?</h4>
            <p style={faqAnswer}>A: </p>

            <h4 style={faqHeading}>Q: Does your school allow extra provision for the students?</h4>
            <p style={faqAnswer}>A: </p>
            <h4 style={faqHeading}>Q: Are your fees refundable?</h4>
            <p style={faqAnswer}>A: </p>
        </>
    );

    let pageContent;
    if (status === 'loading') {
        pageContent = <SuspenseFallback/>;
    }

    pageContent = initContent;

    if (status === 'success' && data?.data?.content !== null) {
        pageContent = <article dangerouslySetInnerHTML={{__html: data?.data?.content}}/>
    }

    return (
        <>
            <PageContainer
                headerImage={HeaderFaq}
                navLinks={NAVLINKS.admission}
                parentLinkText={'Admission'}
                activeLinkText={extractActiveLink(location.pathname, NAVLINKS.admission)}>
                {pageContent}
            </PageContainer>

            <MoveToTop/>
        </>
    );
};

export default FAQs;
