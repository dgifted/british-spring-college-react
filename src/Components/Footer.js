import React, {useRef, useState} from 'react';
import Banner from "../Assets/Logo-img.png";
import Aisa from "../Assets/Group 767.png";
import Twitter from "../Assets/Group 9.svg";
import Face from "../Assets/Group 10.svg";
import Insta from "../Assets/Group 489.svg";
import {useToasts} from 'react-toast-notifications';

import "./Footer.css";
import {Link} from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";

function Footer() {
    const [isLoading, setIsLoading] = useState(false);
    const [emailInput, setEmailInput] = useState('');
    const {addToast} = useToasts();
    // const emailInputRef = useRef(null);

    const onSubmitHandler = (event) => {
        event.preventDefault();
        if (!emailInput || !emailInput.includes('@')) {
            addToast('Please enter valid email', {
                appearance: "warning",
            })
            return;
        }
        setIsLoading(true);

        setTimeout(() => {
            setIsLoading(false);
            setEmailInput('');
            addToast('You have been subscribed to our weekly newsletter.', {
                appearance: "success",
                autoDismiss: true
            });
        }, 1300);
    };

    return (
        <>
            <footer className="footer ">
                <div className="container-lg">
                    <div className="row">
                        <div className=" col-xl-4 col-md-4 col-sm-12 col-xs-12 mb-2 mb-lg-0 Army ">
                            <div className="container-lg useful1">
              <span>
                    <img src={Banner} alt="banner" className="img-fluid img-bann"/>
              </span>
                                <div className="carr-add">
                                    <p className="text-white text-small range">
                                        1, British Spring Estate Road, Nkwelle <br></br> Awka, Anambra State
                                    </p>
                                    <p className="text-white text-small range">
                                        +2347032861442, +2349064479025
                                    </p>
                                    <p className="text-white text-small range">
                                        Springfield Academy, 30 New Nkisi Road,<br></br> GRA OnitshaAnambra state
                                    </p>
                                    <p className="text-white text-small range">
                                        +2347032861442, +2349064479025
                                    </p>
                                </div>

                            </div>

                        </div>


                        <div className=" col-xl-4 col-md-4 col-sm-12 col-xs-12 ">
                            <div className="container-lg usefuls">
                                <header className="h5 text-white lined mb-4">USEFUL LINKS</header>
                                <div className="d">
                                    <ul className="list-unstyled d-flex text-white titing  mr-4 mb-4">
                                        <li className="  mr-4  dxt">
                                            <Link to={'/'} className={'link'}>Home</Link>
                                        </li>
                                        <li className="  mr-4 dxt ">
                                            <Link to={'/about/story-vision-mission'} className={'link'}>About Us</Link>
                                        </li>
                                        <li className="  mr-4 dxt ">
                                            <Link to={'/academics/academic-facilities'}
                                                  className={'link'}>Facilities</Link>
                                        </li>
                                        {/*<li className="  mr-4  dxt">*/}
                                        {/*    Payments*/}
                                        {/*</li>*/}

                                    </ul>
                                    <ul className="list-unstyled d-flex titing  text-white mr-4 mb-4">
                                        <li className="mr-4  dxt">
                                            {/*<Link to={'/admission/apply'} className={'link'}>Apply</Link>*/}
                                            <a href="https://britishspring.educare.school/admission-form" target={'_blank'}
                                            className={'link'}>Apply</a>
                                        </li>
                                        {/*<li className="mr-4 dxt ">*/}
                                        {/*    Admission List*/}
                                        {/*</li>*/}
                                        <li className="mr-4  dxt">
                                            <Link to={'/admission/scholarship'} className={'link'}>Scholarships</Link>
                                        </li>
                                    </ul>
                                </div>
                                <header className="h5 text-white lined mb-4">Follow Us</header>
                                <div className="d">
                                    <ul className="list-unstyled d-flex text-white titing  mr-4 mb-4">
                                        <li className="  mr-4  dxty">
                                            <a href="https://www.facebook.com/britishspringawka"
                                               className={'link'} target={'_blank'}>
                                                <img src={Face} alt="social" className="img-fluid"/>
                                            </a>
                                        </li>
                                        <li className="mr-4 dxty">
                                            <a href="https://twitter.com" className={'link'} target={'_blank'}>
                                                <img src={Twitter} alt="social" className="img-fluid"/>
                                            </a>
                                        </li>
                                        <li className="mr-4 dxty">
                                            <a href="https://www.instagram.com/britishspringcollege"
                                               className={'link'} target={'_blank'}>
                                                <img src={Insta} alt="social" className="img-fluid"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className=" col-xl-4 col-md-4 col-sm-12 col-xs-12  Army ">
                            <div className="container-lg  Gap">
                                <h6 className="use text-white">STAY UP TO DATE WITH <br></br>
                                    THE LATEST NEWS
                                </h6>
                                <div className="controller-input">
                                    <input type="text" onChange={e => setEmailInput(e.target.value)} placeholder="Your email here"
                                           className="form-controlling form" value={emailInput}/>
                                    <button className="submit px-2" disabled={isLoading} onClick={onSubmitHandler}>
                                        Submit{isLoading && (
                                            <span className={'ml-3'}>
                                                <Spinner animation="border" size="sm" variant="danger"/>
                                            </span>
                                    )}
                                    </button>
                                </div>

                                <a href={'http://educare.school/'} target={'_blank'} style={{display: 'inline-block'}}>
                                    <img src={Aisa} alt="Aisa" className="img-fluid">
                                    </img>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div className="divider">
                        <div className="container-lg">
                            <h4>Back to top</h4>
                        </div>
                    </div>
                    <div className="container-lg">
                        <div className="copyrights">
                            <div className=" ">
                                <p className=" mb-0 text-white fullyear">
                                    &copy; {new Date().getFullYear()} {''} British Spring College{' '}
                                </p>
                            </div>
                            <div className="policy">
                                <p>Contact Us</p>
                                <div className="divide"/>
                                <p>Privacy Policy</p>
                            </div>
                        </div>
                    </div>
                </div>

            </footer>

        </>
    )
}

export default Footer
