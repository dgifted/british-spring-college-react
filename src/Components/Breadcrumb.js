import {Link} from "react-router-dom";
import classes from './Breadcrumb.module.css';


const Breadcrumb = (props) => {
    return (
        <ul>
            <li className={classes['home-text']}><Link to={'/'}>Home</Link></li>
            <li>{props.parentLinkText}</li>
            <li>{props.activeLinkText}</li>
        </ul>
    );
};
export default Breadcrumb;
