import React from "react";
import classes from './NavigationOverlay.module.css';

const NavigationOverlay = (props) => {

    return (
        <section className={classes['menu-overlay']}
                    onMouseEnter={props.onMouseEnterHandler}
                    onMouseLeave={props.onMouseLeaveHandler}>
            <div className={classes['menu-container']}>{props.children}</div>
        </section>
    );
}

export default NavigationOverlay;
