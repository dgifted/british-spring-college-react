import {useEffect} from 'react';
import {useLocation} from 'react-router-dom';

const MoveToTop = () => {
    const routePath = useLocation();
    const onTop = () => {
        window.scroll(0,0);
    };

    useEffect(() => {
        onTop();

    }, [routePath]);

    return null;
};

export default MoveToTop;
