import React, {Component} from "react";
import Slider from "react-slick";
import {Link} from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slide from "../Assets/images/Group 3.png";
import Slide1 from "../Assets/images/Group 4.png";
import Slide2 from "../Assets/images/Group 5.png";
import Slide3 from "../Assets/images/Group 45.png";
import Slide4 from "../Assets/images/Group 76.png";
import Slide5 from "../Assets/images/Group 86.png";
import Slide6 from "../Assets/images/Group 87.png";
import Slide7 from "../Assets/images/Group 390.png";
import Card from 'react-bootstrap/Card'
import Virtual from "../Assets/Virtual-tour.png";
import Direct from "../Assets/Directors.png";
import Group from "../Assets/Group-stud.png";
import Truly from "../Assets/Multicultural.png";
import Award from "../Assets/Groupy508.png";
import Visit from "../Assets/Visit-us.png";
import Video from "../Assets/Vid-group.png"
import Women from "../Assets/Womens.png"
import AdmissionList from '../Assets/documents/BSC Admission List 2021-2022.pdf';
import NewsLetter from '../Assets/documents/BSC NEWSLETTER MARCH 2021-1.pdf';
import "./CarSlide.css"

export default class CarSlider extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            cssEase: 'ease',
            autoplaySpeed: 10000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: 2000,
        };
        return (
            <>
                <div>
                    <Slider {...settings}>
                        <div>
                            <img src={Slide} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide1} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide2} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide3} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide4} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide5} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide6} alt="Log" className="img-fluid"></img>
                        </div>
                        <div>
                            <img src={Slide7} alt="Log" className="img-fluid"></img>
                        </div>
                    </Slider>
                </div>

                <section>
                    <div className="Name-head">
                        <h1>
                            Welcome to British Spring College
                        </h1>
                        <center>
                            <hr></hr>
                        </center>
                        <p>
                            Our vision is to raise a generation of students with the right tools, skills, moral and
                            mental abilities to excel in life and contribute positively to the development of mankind.
                        </p>
                    </div>

                </section>

                <section className="shift-up">
                    <div className="container-fluid">
                        <div className="row mt-5 cont-move">
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div className="Keep" style={{backgroundImage: `url(${Direct})`}}>
                                    <h1>Director’s Welcome</h1>
                                    <span className="overlay-7">
                                        <h2>Director’s Welcome</h2>
                                        <p>
                                            Welcome to British Spring College’s website. Thank you for taking time to visit us. We hope the links provide a glimpse of our brand of excellence.
                                        </p>
                                        {/*<h4>Read more</h4>*/}
                                        <Link to={'/about/director-welcome'} className={'link'}>Read more</Link>
                                    </span>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div className="Keep" style={{backgroundImage: `url(${Virtual})`}}>
                                    <h1>Virtual tour</h1>
                                    <span className="overlay-7">
                                        <h2>Virtual tour</h2>
                                        <p>Explore BSC on our virtual tour, and through various videos. Discover our outstanding facilities amidst glorious surroundings.</p>
                                        {/*<h4>Read more</h4>*/}
                                        <Link to={'/about/virtual-tour'} className={'link'}>Read more</Link>
                                    </span>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div className="Keep" style={{backgroundImage: `url(${Group})`}}>
                                    <h1> PARENTS’ AREA </h1>
                                    <span className="overlay-7">
                                        <h2> PARENTS’ AREA </h2>
                                        <p>
                                            Essential Informations for Parents can be found here.
                                        </p>
                                        {/*<h4>Read more</h4>*/}
                                        {/*<Link to={'/admission/apply'} className={'link'}>Read more</Link>*/}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="row  cont-move">
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div className="Keep" style={{backgroundImage: `url(${Truly})`}}>
                                    <h1>Truly Multicultural</h1>
                                    <span className="overlay-7">
                                        <h2>Truly Multicultural</h2>
                                        <p>At BSC we are a culturally diverse community which fosters a passion and enthusiasm for learning, through outstanding educational practices.</p>
                                        {/*<h4>Read more</h4>*/}
                                        <Link to={'/life/cultural-activities'} className={'link'}>Read more</Link>
                                    </span>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div className="Keep" style={{backgroundImage: `url(${Award})`}}>
                                    <h1>Academic Excellence</h1>
                                    <span className="overlay-7">
                                        <h2>Academic Excellence</h2>
                                        <p>At BSC we provide a happy, caring and stimulating environment, allowing you to extend your intellectual and emotional development.</p>
                                        {/*<h4>Read more</h4>*/}
                                        <Link to={'/academics/academic-methods'} className={'link'}>Read more</Link>
                                    </span>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <div className="Keep" style={{backgroundImage: `url(${Visit})`}}>
                                    <h1>VISIT US</h1>
                                    <span className="overlay-7">
                                        <h2>VISIT US</h2>
                                        <p>We are delighted that you are considering BSC’s for your son or daughter. Book a visit day with us today.</p>
                                        {/*<h4>Read more</h4>*/}
                                        {/*<Link to={'/'} className={'link'}>Read more</Link>*/}
                                        <a href="https://britishspring.educare.school/enquiry" target={'_blank'} className={'link'}>Read more</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>

                    <div className="container-lg connected">
                        <section>

                            <span className="Name-head cont">
                                <h1>   Stay Connected   </h1>
                                <center><hr></hr></center>
                            </span>

                        </section>
                        <div className="row">
                            <div className="col-md-4 col-lg-4 col-sm-12">
                                <span className="Name-head1">
                                </span>
                                <Card className="stretch-card">
                                    <Card.Body>
                                        <Card.Title style={{fontWeight: "semi-bold", fontSize: "26px",}}>School
                                            News</Card.Title>
                                        <center>
                                            <hr></hr>
                                        </center>
                                        <Card.Subtitle className="mb-2 "
                                                       style={{fontWeight: "bold", fontSize: "22px",}}>ADMISSION LIST
                                            INTO YEAR 7, 2021/2022 ACADEMIC SESSION IS OUT!!!</Card.Subtitle>
                                        <Card.Text>
                                            Names of successful applicants that have been admitted into year 7,
                                            2021/2022 academic session is OUT!!!
                                        </Card.Text>
                                        <div className="but-links">
                                            <Card.Link href="#" className="first-link">19th April 2021</Card.Link>
                                            <Card.Link href={AdmissionList} className="sec-link" target={'_blank'}>View here</Card.Link>
                                        </div>
                                    </Card.Body>
                                    <Card.Body>
                                        <Card.Subtitle className="mb-2 "
                                                       style={{fontWeight: "bold", fontSize: "22px",}}>BRITISH SPRING
                                            COLLEGE NEWSLETTER</Card.Subtitle>
                                        <Card.Text>
                                            With delight, we send our season’s greetings. We thank God for having seen
                                            us through this term; the second one of this academic session..
                                        </Card.Text>
                                        <div className="but-links">
                                            <Card.Link href="#" className="first-link">31st March 2021</Card.Link>
                                            <Card.Link href={NewsLetter} className="sec-link" target={'_blank'}>Read more</Card.Link>
                                        </div>
                                    </Card.Body>
                                    {/*<Card.Body className="bunt">*/}
                                        {/*<Card.Subtitle className="mb-2 "></Card.Subtitle>*/}
                                        {/* <Card.Text> </Card.Text> */}
                                        {/*<Card.Link href="#" className="sec-link1">Next</Card.Link>*/}
                                    {/*</Card.Body>*/}
                                </Card>
                            </div>

                            <div className="col-md-8 col-lg-8 col-sm-12">
                                <span className="Name-head1 hider">
                                    <h1>Facebook</h1>
                                </span>
                                <a href={"https://www.facebook.com/britishspringawka"}
                                   className={'cardWrapLink'} target={'_blank'}>
                                    <div className="card-in hide .d-sm-none .d-md-block">
                                        <img src={Video} alt="Video" className="img-card img-fluid"></img>
                                        <span className="body-card">
                                        <div className="ann-card1">
                                            <div className="ann-card">
                                                <img src={Video} alt="Video"
                                                     className="img-card shed-img img-fluid"></img>
                                                <div className="British">
                                                    <h5>British Spring College, Awka</h5>
                                                    <span className={'socialHandleText'}>@britishspringawka</span>
                                                </div>
                                            </div>
                                            <div className="date--card">
                                                <h4>Mar 8</h4>
                                            </div>
                                        </div>
                                        <div className="text-card">
                                            <h5>
                                                BSC Admission window is still open. Watch the video attached for more details.
                                            </h5>
                                        </div>
                                    </span>
                                    </div>
                                </a>
                                <div className="row">
                                    <div className="col-md-5 col-lg-5 col-sm-12">
                                        <span className="Name-head1">
                                            <h1>Twitter</h1>
                                        </span>
                                        <Card className="Vid-card">
                                            <Card.Img variant="top" className="img-fluid" src={Video}/>
                                            <span className="body-card1">
                                                <div className="ann-card1">
                                                    <div className="ann-card12">
                                                        <img src={Video} alt="Video"
                                                             className="img-card shed-img img-fluid"></img>
                                                        <div className="clan1">
                                                            <h5>British Spring College, Awka</h5>
                                                            <span className={'socialHandleText'}>@britishspringawka</span>
                                                        </div>
                                                    </div>
                                                    <div className="date-card">
                                                        <h4>Mar 11</h4>
                                                    </div>
                                                </div>
                                                <div className="text-card1">
                                                    <h5>
                                                        BSC Admission window is still open. Watch the video attached for more details.
                                                    </h5>
                                                </div>
                                            </span>
                                        </Card>
                                    </div>
                                    <div className="col-2 hider"/>
                                    <div className="col-md-5  col-lg-5 col-sm-12">
                                        <span className="Name-head1">
                                            <h1>Instagram</h1>
                                        </span>
                                        <a href="https://www.instagram.com/britishspringcollege/"
                                           className={'cardWrapLink'} target={'_blank'}>
                                            <Card className="Vid-card">
                                                <Card.Img variant="top" className="img-fluid" src={Women}/>
                                                <span className="body-card1">
                                                <div className="ann-card1">
                                                    <div className="ann-card12">
                                                        <img src={Video} alt="Video"
                                                             className="img-card shed-img img-fluid"></img>
                                                        <div className="clan1">
                                                            <h5>British Spring College, Awka</h5>
                                                            <span className={'socialHandleText'}>@britishspringawka</span>
                                                        </div>
                                                    </div>
                                                    <div className="date-card">
                                                        <h4>Mar 8</h4>
                                                    </div>
                                                </div>
                                                <div className="text-card1">
                                                    <h5>
                                                        We celebrate all women in the world. Happy women’s Day
                                                    </h5>
                                                </div>
                                            </span>
                                            </Card>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
