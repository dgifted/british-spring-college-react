import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import classes from './AdmissionForm.module.css';
import Button from "react-bootstrap/Button";
import {useState} from "react";
import Spinner from "react-bootstrap/Spinner";
import {useToasts} from 'react-toast-notifications';
import Modal from "react-bootstrap/Modal";
import {useHistory} from 'react-router-dom';
import ApplyConfirmModal from "./ApplyConfirmModal";

const AdmissionForm = (props) => {
    const [firstNameInput, setFirstNameInput] = useState('');
    const [middleNameInput, setMiddleNameInput] = useState('');
    const [lastNameInput, setLastNameInput] = useState('');
    const [homeAddressInput, setHomeAddressInput] = useState('');
    const [cityInput, setCityInput] = useState('');
    const [stateInput, setStateInput] = useState('');
    const [dateOfBirthInput, setDateOfBirthInput] = useState('');
    const [stateOfOriginInput, setStateOfOriginInput] = useState('');
    const [nationalityInput, setNationalityInput] = useState('');
    const [religionInput, setReligionInput] = useState('');
    const [genderInput, setGenderInput] = useState('female');
    const [nativeLanguageInput, setNativeLanguageInput] = useState('');
    const [entryClassInput, setEntryClassInput] = useState('');

    const [fTitleInput, setFTitleInput] = useState('');
    const [fFirstNameInput, setFFirstNameInput] = useState('');
    const [fMiddleNameInput, setFMiddleNameInput] = useState('');
    const [fLastNameInput, setFLastNameInput] = useState('');
    const [fatherOccupationInput, setFatherOccupationInput] = useState('');
    const [fatherMaritalStatusInput, setFatherMaritalStatusInput] = useState('');
    const [fChildCustodyInput, setFChildCustodyInput] = useState('');
    const [fHomeAddressInput, setFHomeAddressInput] = useState('');
    const [fCityInput, setFCityInput] = useState('');
    const [fStateInput, setFStateInput] = useState('');
    const [fEmailInput, setFEmailInput] = useState('');
    const [fPhoneInput, setFPhoneInput] = useState('');

    const [mTitleInput, setMTitleInput] = useState('');
    const [mFirstNameInput, setMFirstNameInput] = useState('');
    const [mMiddleNameInput, setMMiddleNameInput] = useState('');
    const [mLastNameInput, setMLastNameInput] = useState('');
    const [motherOccupationInput, setMotherOccupationInput] = useState('');
    const [motherMaritalStatusInput, setMotherMaritalStatusInput] = useState('');
    const [mChildCustodyInput, setMChildCustodyInput] = useState('');
    const [mHomeAddressInput, setMHomeAddressInput] = useState('');
    const [mCityInput, setMCityInput] = useState('');
    const [mStateInput, setMStateInput] = useState('');
    const [mEmailInput, setMEmailInput] = useState('');
    const [mPhoneInput, setMPhoneInput] = useState('');

    const [step, setStep] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const {addToast} = useToasts();
    const formErrorMessage = 'Some important field have been omitted.';

    const validateForm = (step) => {
        if (step === 1) {
            const formStep1State = [
                firstNameInput,
                middleNameInput,
                lastNameInput,
                homeAddressInput,
                cityInput,
                stateInput,
                dateOfBirthInput,
                stateOfOriginInput,
                nationalityInput,
                religionInput,
                genderInput,
                nativeLanguageInput,
                entryClassInput
            ];

            formStep1State.forEach(state => {
                if (state === '') {
                    console.log('Null state ', state);
                    return false;
                }

                return true;
            });
        }

        if (step === 2) {
            const formStep2State = [
                fFirstNameInput,
                fMiddleNameInput,
                fLastNameInput,
                fatherOccupationInput,
                fatherMaritalStatusInput,
                fChildCustodyInput,
                fHomeAddressInput,
                fCityInput,
                fStateInput,
                fEmailInput,
                fPhoneInput,
                mFirstNameInput,
                mMiddleNameInput,
                mLastNameInput,
                motherOccupationInput,
                motherMaritalStatusInput,
                mChildCustodyInput,
                mHomeAddressInput,
                mCityInput,
                mStateInput,
                mEmailInput,
                mPhoneInput
            ];

            formStep2State.forEach(state => {
                if (state === null) {
                    return false;
                }

                return true;
            });

        }

        return false;
    };

    const onPrevClickHandler = () => {
        if (step === 1)
            return;
        setStep(prevState => prevState - 1);
    };

    const onNextClickHandler = () => {
        if (step === 3)
            return;

        setStep(prevState => prevState + 1);
    };

    const onFormSubmitHandler = (evt) => {
        evt.preventDefault();
        setIsLoading(true)
        setTimeout(() => {
            setIsLoading(false);
            setFormSubmitted(true);
        }, 2000)
    };

    const emListsStyle = {
        textAlign: 'left',
        paddingLeft: '2rem',
    };

    const emListsItemStyle = {
        marginTop: '1rem',
        fontSize: '20px',
        fontFamily: 'Gill'
    };

    const paraLinesStyle = {
        lineHeight: '200%'
    };

    const onConfirmationModalClosed = () => {
        setFormSubmitted(false);
        setStep(1);
    }

    return (
        <Card>
            <Card.Header>
                <Card.Title>BRITISH SPRING COLLEGE ADMISSION</Card.Title>
            </Card.Header>

            <Card.Body className={classes.formWrapper}>
                {step === 1 && (
                    <>
                        <h3>INSTRUCTION.</h3>
                        <p>Before you commence Application, Please ensure you have scan copies of the following
                            requirements:</p>
                        <ol style={emListsStyle}>
                            <li style={emListsItemStyle}>Recent Coloured Passport Photography</li>
                            <li style={emListsItemStyle}>Birth Certificate</li>
                            <li style={emListsItemStyle}>Last Class Report Sheet</li>
                            <li style={emListsItemStyle}>Medical Certificate from a recognized Hospital</li>
                        </ol>
                    </>
                )}

                <Form className={'mb-5'}>
                    {step === 1 && (
                        <>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Row>
                                    <Col sm={12} md={4}>
                                        <Form.Control type={'text'}
                                                      name={'firstName'}
                                                      onChange={(e) => setFirstNameInput(e.target.value)}
                                                      placeholder={'First name'}
                                                      size={'lg'}
                                                      value={firstNameInput}/>
                                    </Col>
                                    <Col sm={12} md={4}>
                                        <Form.Control type={'text'}
                                                      name={'middleName'}
                                                      onChange={(e) => setMiddleNameInput(e.target.value)}
                                                      placeholder={'Middle name'}
                                                      size={'lg'}
                                                      value={middleNameInput}/>
                                    </Col>
                                    <Col sm={12} md={4}>
                                        <Form.Control type={'text'}
                                                      name={'lastName'}
                                                      onChange={(e) => setLastNameInput(e.target.value)}
                                                      placeholder={'Last name'}
                                                      size={'lg'}
                                                      value={lastNameInput}/>
                                    </Col>
                                </Row>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Address</Form.Label>
                                <Form.Control type={'text'}
                                              name={'homeAddress'}
                                              onChange={(e) => setHomeAddressInput(e.target.value)}
                                              placeholder={'Residential address'}
                                              size={'lg'} value={homeAddressInput}/><br/>
                                <Form.Control type={'text'}
                                              name={'city'}
                                              onChange={e => setCityInput(e.target.value)}
                                              placeholder={'City'}
                                              size={'lg'}
                                              value={cityInput}/><br/>
                                <Form.Control type={'text'}
                                              name={'state'}
                                              onChange={e => setStateInput(e.target.value)}
                                              placeholder={'State'}
                                              size={'lg'}
                                              value={stateInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Date of birth</Form.Label>
                                <Form.Control type={'date'}
                                              className={classes.selectControl}
                                              name={'dateOfBirth'}
                                              onChange={e => setDateOfBirthInput(e.target.value)}
                                              size={'lg'} value={dateOfBirthInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>State of origin</Form.Label>
                                <Form.Control type={'text'}
                                              className={classes.selectControl}
                                              name={'stateOfOrigin'}
                                              onChange={e => setStateOfOriginInput(e.target.value)}
                                              size={'lg'} value={stateOfOriginInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Nationality</Form.Label>
                                <Form.Control as={'select'}
                                              className={classes.selectControl}
                                              name={'nationality'}
                                              onChange={e => setNationalityInput(e.target.value)}
                                              size={'lg'}>
                                    <option>Ghana</option>
                                    <option>Nigeria</option>
                                </Form.Control>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Religion</Form.Label>
                                <Form.Control type={'text'}
                                              className={classes.selectControl}
                                              name={'religion'}
                                              onChange={e => setReligionInput(e.target.value)}
                                              size={'lg'} value={religionInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Gender</Form.Label>
                                <Form.Check type={'radio'}
                                            checked={genderInput === 'female'}
                                            label={'Female'}
                                            name={'gender'}
                                            onChange={() => setGenderInput('female')}
                                            value={'female'}/>
                                <Form.Check type={'radio'}
                                            checked={genderInput !== 'female'}
                                            label={'Male'}
                                            name={'gender'}
                                            onChange={() => setGenderInput('male')}
                                            value={'male'}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Language spoken at home</Form.Label>
                                <Form.Control type={'text'}
                                              className={classes.selectControl}
                                              name={'nativeLanguage'}
                                              onChange={e => setNativeLanguageInput(e.target.value)}
                                              size={'lg'}
                                              value={nativeLanguageInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Class of entry</Form.Label>
                                <Form.Control as={'select'}
                                              className={classes.selectControl}
                                              name={'entryClass'}
                                              onChange={e => setEntryClassInput(e.target.value)}
                                              size={'lg'}>
                                    <option>JSS 1</option>
                                    <option>JSS 2</option>
                                </Form.Control>
                            </Form.Group>
                        </>
                    )}

                    {step === 2 && (
                        <>
                            <div> {/*For father*/}
                                <h3>Father's Details</h3>
                                <Form.Group>
                                    <Form.Label>Father's name</Form.Label>
                                    <Row>
                                        <Col sm={12} md={3}>
                                            <Form.Control as={'select'}
                                                          name={'fTitle'}
                                                          onChange={e => setFTitleInput(e.target.value)}
                                                          size={'lg'}>
                                                <option value={'mr'}>Mr.</option>
                                                <option value={'barr'}>Barr.</option>
                                                <option value={'gen'}>Gen.</option>
                                                <option value={'sir'}>Sir</option>
                                                <option value={'master'}>Master</option>
                                                <option value={'prof'}>Prof</option>
                                                <option value={'rep'}>Rep</option>
                                                <option value={'sen'}>Sen</option>
                                                <option value={'st'}>St</option>
                                            </Form.Control>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'fFirstName'}
                                                          onChange={(e) => setFFirstNameInput(e.target.value)}
                                                          placeholder={'First name'}
                                                          size={'lg'}
                                                          value={fFirstNameInput}/>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'fMiddleName'}
                                                          onChange={(e) => setFMiddleNameInput(e.target.value)}
                                                          placeholder={'Middle name'}
                                                          size={'lg'}
                                                          value={fMiddleNameInput}/>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'fLastName'}
                                                          onChange={(e) => setFLastNameInput(e.target.value)}
                                                          placeholder={'Last name'}
                                                          size={'lg'}
                                                          value={fLastNameInput}/>
                                        </Col>
                                    </Row>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Father's occupation.</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'fatherOccupation'}
                                                  onChange={e => setFatherOccupationInput(e.target.value)}
                                                  size={'lg'}
                                                  value={fatherOccupationInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Father's marital status.</Form.Label>
                                    <Form.Control as={'select'}
                                                  name={'fatherMaritalStatus'}
                                                  onChange={e => setFatherMaritalStatusInput(e.target.value)}
                                                  size={'lg'}>
                                        <option value={'married'}>Married</option>
                                        <option value={'divorced'}>Divorced</option>
                                        <option value={'widowed'}>Windowed</option>
                                    </Form.Control>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>If Divorced, who has custody of the Children?</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'fChildCustody'}
                                                  onChange={e => setFChildCustodyInput(e.target.value)}
                                                  size={'lg'}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Father's Address</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'fHomeAddress'}
                                                  onChange={(e) => setFHomeAddressInput(e.target.value)}
                                                  placeholder={'Residential address'}
                                                  size={'lg'} value={fHomeAddressInput}/><br/>
                                    <Form.Control type={'text'}
                                                  name={'fCity'}
                                                  onChange={e => setFCityInput(e.target.value)}
                                                  placeholder={'City'}
                                                  size={'lg'}
                                                  value={fCityInput}/><br/>
                                    <Form.Control type={'text'}
                                                  name={'fState'}
                                                  onChange={e => setFStateInput(e.target.value)}
                                                  placeholder={'State'}
                                                  size={'lg'}
                                                  value={fStateInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Father's email address</Form.Label>
                                    <Form.Control type={'email'}
                                                  name={'fEmail'}
                                                  onChange={e => setFEmailInput(e.target.value)}
                                                  size={'lg'}
                                                  value={fEmailInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Father's mobile number</Form.Label>
                                    <Form.Control type={'tel'}
                                                  name={'fPhone'}
                                                  onChange={e => setFPhoneInput(e.target.value)}
                                                  size={'lg'}
                                                  value={fPhoneInput}/>
                                </Form.Group>
                            </div>

                            <div> {/*For mother*/}
                                <h3>Mother's Details</h3>
                                <Form.Group>
                                    <Form.Label>Mother's name</Form.Label>
                                    <Row>
                                        <Col sm={12} md={3}>
                                            <Form.Control as={'select'}
                                                          name={'mTitle'}
                                                          onChange={e => setMTitleInput(e.target.value)}
                                                          size={'lg'}>
                                                <option value={'mrs'}>Mrs.</option>
                                                <option value={'barr'}>Barr.</option>
                                                <option value={'lady'}>Lady</option>
                                                <option value={'prof'}>Prof</option>
                                                <option value={'rep'}>Rep</option>
                                                <option value={'sen'}>Sen</option>
                                                <option value={'st'}>St</option>
                                            </Form.Control>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'mFirstName'}
                                                          onChange={(e) => setMFirstNameInput(e.target.value)}
                                                          placeholder={'First name'}
                                                          size={'lg'}
                                                          value={mFirstNameInput}/>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'mMiddleName'}
                                                          onChange={(e) => setMMiddleNameInput(e.target.value)}
                                                          placeholder={'Middle name'}
                                                          size={'lg'}
                                                          value={mMiddleNameInput}/>
                                        </Col>
                                        <Col sm={12} md={3}>
                                            <Form.Control type={'text'}
                                                          name={'mLastName'}
                                                          onChange={(e) => setMLastNameInput(e.target.value)}
                                                          placeholder={'Last name'}
                                                          size={'lg'}
                                                          value={mLastNameInput}/>
                                        </Col>
                                    </Row>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Mother's occupation.</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'motherOccupation'}
                                                  onChange={e => setMotherOccupationInput(e.target.value)}
                                                  size={'lg'}
                                                  value={motherOccupationInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Mother's marital status.</Form.Label>
                                    <Form.Control as={'select'}
                                                  name={'motherMaritalStatus'}
                                                  onChange={e => setMotherMaritalStatusInput(e.target.value)}
                                                  size={'lg'}>
                                        <option value={'married'}>Married</option>
                                        <option value={'divorced'}>Divorced</option>
                                        <option value={'widowed'}>Windowed</option>
                                    </Form.Control>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>If Divorced, who has custody of the Children?</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'mChildCustody'}
                                                  onChange={e => setMChildCustodyInput(e.target.value)}
                                                  size={'lg'}
                                                  value={mChildCustodyInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Mother's Address</Form.Label>
                                    <Form.Control type={'text'}
                                                  name={'mHomeAddress'}
                                                  onChange={(e) => setMHomeAddressInput(e.target.value)}
                                                  placeholder={'Residential address'}
                                                  size={'lg'} value={mHomeAddressInput}/><br/>
                                    <Form.Control type={'text'}
                                                  name={'mCity'}
                                                  onChange={e => setMCityInput(e.target.value)}
                                                  placeholder={'City'}
                                                  size={'lg'}
                                                  value={mCityInput}/><br/>
                                    <Form.Control type={'text'}
                                                  name={'mState'}
                                                  onChange={e => setMStateInput(e.target.value)}
                                                  placeholder={'State'}
                                                  size={'lg'}
                                                  value={mStateInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Mother's email address</Form.Label>
                                    <Form.Control type={'email'}
                                                  name={'mEmail'}
                                                  onChange={e => setMEmailInput(e.target.value)}
                                                  size={'lg'}
                                                  value={mEmailInput}/>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Mother's mobile number</Form.Label>
                                    <Form.Control type={'tel'}
                                                  name={'mPhone'}
                                                  onChange={e => setMPhoneInput(e.target.value)}
                                                  size={'lg'}
                                                  value={mPhoneInput}/>
                                </Form.Group>
                            </div>
                        </>
                    )}

                    {step === 3 && (
                        <>
                            <h3>Health History</h3>
                            <Form.Group>
                                <Form.Label>Any Physical Defects? (Please state)</Form.Label>
                                <Form.Control as={'textarea'} rows={5}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Does your child wear glasses?</Form.Label>
                                <Form.Control as={'textarea'} rows={5}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Does your Child suffer any Allergy?</Form.Label>
                                <Form.Check type={'checkbox'} label={'Food'} value={'food'}/>
                                <Form.Check type={'checkbox'} label={'Medicine'} value={'medicine'}/>
                                <Form.Check type={'checkbox'} label={'Others, specify:'} value={''}/>
                                <Form.Control as={'textarea'} rows={5}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label className={'text-capitalize'}>Any Other Health Problem? (Please
                                    Specify)?</Form.Label>
                                <Form.Control as={'textarea'} rows={5}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Recent Colour Passport photography (Scan copy)</Form.Label>
                                <Form.Control type={'file'}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Birth Certificate (scan copy)</Form.Label>
                                <Form.Control type={'file'}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Last Class Report Sheet (scan copy)</Form.Label>
                                <Form.Control type={'file'}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Medical Certificate from a recognized Hospital (scan copy)</Form.Label>
                                <Form.Control type={'file'}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>For Transfer Students, Past Secondary School Results (Scan
                                    copy)</Form.Label>
                                <Form.Control type={'file'}/>
                            </Form.Group>
                        </>
                    )}
                </Form>

                <div className={'d-flex justify-content-around'}>
                    {step !== 1 && (
                        <Button onClick={onPrevClickHandler} variant={'secondary'} size={'lg'}>Prev</Button>)}
                    {step !== 3 && (
                        <Button onClick={onNextClickHandler} variant={'secondary'} size={'lg'}>Next</Button>)}
                    {step === 3 && (<Button onClick={onFormSubmitHandler} variant={'primary'} size={'lg'}>
                        Submit Application{isLoading && (
                        <span className={'ml-2'}>
                                <Spinner animation="border" role="status" size={'sm'}>
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                            </span>
                    )}
                    </Button>)}
                </div>
            </Card.Body>

            <ApplyConfirmModal show={formSubmitted} onHide={onConfirmationModalClosed}/>
        </Card>
    );
};

export default AdmissionForm;
