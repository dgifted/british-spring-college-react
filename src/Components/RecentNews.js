import React from "react";
import {Link} from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import {useQuery} from "react-query";

const fetchRecentNews = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/news/recent`);
    return res.json();
};

const RecentNews = () => {
    const {data, status} = useQuery('recentNews', fetchRecentNews);

    let pageContent;

    if (status === 'loading') {
        pageContent = <div style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'stretch',
            alignItems: 'center',
            backgroundColor: 'white'
        }}>
            <Spinner animation={'border'} variant={'secondary'}/>
        </div>;
    }

    if (status === "error") {
        pageContent = <div className={'text-center'}>Could not fetch news now.</div>
    }

    if (status === "success" && data?.data?.length === 0) {
        pageContent = <div className={'text-center'}>No recent news</div>
    }

    if (status === "success" && data?.data?.length > 0) {
        pageContent = (
            <div className={'text-left d-flex flex-column'}>
                {data?.data?.map(news => (
                    <Link to={`/about/school-news/${news.id}`} key={news.id} className={'text-muted'} style={{marginBottom: '1rem'}}>{news.title}</Link>
                ))}
            </div>
        );
    }

    return (
        <>{pageContent}</>
    );
};

export default RecentNews;
