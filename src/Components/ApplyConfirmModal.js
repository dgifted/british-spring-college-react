import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";


const ApplyConfirmModal = (props) => {

    return (
        <Modal {...props} size={'sm'}>
            <Modal.Header closeButton/>
            <Modal.Body>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                    <h4 className={'text-success'}>Application has been successfully submitted.</h4>
                    <p className={'mt-2 mb-5 text-center'}>We will get in touch with you as soon as we finish reviews. <br/>Thank you.</p>
                    <Button onClick={props.onHide} variant={'secondary'} size={'lg'}>Close</Button>
                </div>
            </Modal.Body>
        </Modal>
    );
};

export default ApplyConfirmModal;
