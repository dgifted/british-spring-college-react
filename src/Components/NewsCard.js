import React from "react";
import classes from './NewsCard.module.css';
import {Link} from "react-router-dom";

const NewsCard = (props) => {
    let newsBanner = `https://picsum.photos/id/1023/900/300`;

    // if (props.event.image) {
    //     eventImage = `${process.env.REACT_APP_SERVER_URL}/images/${props.event.image}`;
    // }

    return <div className={classes['event-card']}>
        <div className={classes['thumbnail']} style={{
            background: `url(${newsBanner}) center/cover no-repeat padding-box`
        }}/>

        <h3 className={classes['event-card-title']}>{props.news.title}</h3>

        <span className={classes['event-date']}>
            <b>Added on: </b>
            {new Date(props.news['created_at']).toDateString()}
            </span>

        <div className={classes['content']} dangerouslySetInnerHTML={{__html: props.news.content}}/>

        <div style={{height: '42px', width: '100%', backgroundColor: 'white'}}>&nbsp;</div>
        <div className={classes['read-more-wrapper']}>
            <Link to={`/about/school-news/${props.news.id}`} className={classes['read-more']}>Read More</Link>
        </div>
    </div>;
};

export default NewsCard;
