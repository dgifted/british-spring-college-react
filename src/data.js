import FemaleRoom from './Assets/images/female_room.jpeg';
import HostelLounge1 from './Assets/images/hostel_lounge1.jpeg';
import LoungeViewFemale from './Assets/images/lounge_view_female.jpeg';
import LoungeView from './Assets/images/lounge_view.jpeg';

const hostelFacilities = [
    {
        caption: 'Female room',
        picture: FemaleRoom
    },
    {
        caption: 'Hostel lounge 1',
        picture: HostelLounge1
    },
    {
        caption: 'Lounge view female',
        picture: LoungeViewFemale
    },
    {
        caption: 'Lounge View',
        picture: LoungeView
    }
];

export default hostelFacilities;