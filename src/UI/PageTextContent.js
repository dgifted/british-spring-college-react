import React from "react";

import classes from './PageTextContent.module.css';

const PageTextContent = (props) => {
    return <>
        {props.children}
    </>
};
export default PageTextContent;
