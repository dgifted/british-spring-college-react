import {NavLink} from "react-router-dom";

import classes from './PageContainer.module.css';
import Breadcrumb from "../Components/Breadcrumb";

const PageContainer = (props) => {
    return (
        <section className={classes.wrapper}>
            <section>
                <div className={classes['header-img']}>
                    <img src={props.headerImage} alt="Header" className="img-fluid"/>
                </div>
            </section>

            <section>
                <div className={'container-md'}>
                    <div className={classes['note-lists']}>
                        <ul>
                            {props.navLinks.map((link, index) =>
                                <li key={index}>
                                    <NavLink to={link.path} activeClassName={classes['active-link']}>{link.text}</NavLink>
                                </li>)
                            }
                        </ul>
                    </div>
                    <div className={classes['bread-crumbs']}>
                        <div className={classes['bold']}>You are here:</div>
                        <Breadcrumb
                            parentLinkText={props.parentLinkText}
                            activeLinkText={props.activeLinkText}/>
                    </div>
                </div>
            </section>
            <section className="container-md">
                <div className="comments">
                    <div className="intro">
                        <small>{props.parentLinkText}</small>
                        <header>{props.activeLinkText}</header>
                    </div>
                    <div className="row create">
                        <div className="col-sm-12 col-md-11 col-lg-11 make">
                            <div className="speech">
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    );
};

export default PageContainer;
