import React from "react";
import {Container, Col, Row, Card} from "react-bootstrap";
import RecentNews from "../Components/RecentNews";

import classes from './NewsLayout.module.css'

const NewsLayout = (props) => {

    return (
        <Container>
            <Row>
                <Col sm={12} md={8} className={classes['content-display']}>
                    {props.children}
                </Col>

                <Col sm={12} md={4}>
                    <Card className={'px-0'}>
                        <Card.Header>
                            <Card.Title className={classes['recent-new-title']}>Recent News & Updates</Card.Title>
                        </Card.Header>

                        <Card.Body>
                            <RecentNews/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

export default NewsLayout;
