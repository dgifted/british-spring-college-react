import Card from 'react-bootstrap/Card';
import classes from './FacilityCard.module.css';

const FacilityCard = (props) => {

    return (
        <Card className={classes['facility-card']}>
            <Card.Body className={classes['facility-card-body']}>
                <img src={props.facility.picture} alt={''}/>
                <p>{props.facility.caption}</p>
            </Card.Body>
        </Card>
    );
};

export default FacilityCard;