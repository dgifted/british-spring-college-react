const NAVLINKS = {
    about: [
        {text: 'Director\'s Welcome', path: 'director-welcome'},
        {text: 'Our Story, Vision & Mission', path: 'story-vision-mission'},
        {text: 'Calendar', path: 'calendar'},
        {text: 'School Policies', path: 'school-policies'},
        {text: 'Virtual Tour', path: 'virtual-tour'},
        {text: 'Achievements', path: 'achievements'},
        {text: 'Contact & Directions', path: 'contact-directions'},
        {text: 'School News', path: 'school-news'},
        // {text: 'School News', path: 'school-news/detail'},
    ],
    admission: [
        {text: 'Admission Procedure', path: 'admission-procedure'},
        {text: 'School Tuition Fees', path: 'tuition-fees'},
        {text: 'Boarding', path: 'boarding'},
        {text: 'Scholarship', path: 'scholarship'},
        {text: 'FAQ', path: 'faq'}
    ],
    academics: [
        {text: 'Academic Facilities', path: 'academic-facilities'},
        {text: 'Curriculum', path: 'curriculum'},
        {text: 'Library', path: 'library'},
        {text: 'Academic Methods', path: 'academic-methods'}
    ],
    life: [
        {text: 'Cultural Activities', path: 'cultural-activities'},
        {text: 'Music', path: 'music'},
        {text: 'Food & Nutrition', path: 'food-nutrition'},
        {text: 'Sports', path: 'sports'},
        {text: 'Vocational Activities', path: 'vocational-activities'}
    ]
};
export default NAVLINKS;
